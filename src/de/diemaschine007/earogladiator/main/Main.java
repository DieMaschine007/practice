package de.diemaschine007.earogladiator.main;
 
import de.diemaschine007.earogladiator.arenasystem.Arena;
import de.diemaschine007.earogladiator.arenasystem.ArenaManager;
import de.diemaschine007.earogladiator.arenasystem.ArenaType;
import de.diemaschine007.earogladiator.arenasystem.Arenas;
import de.diemaschine007.earogladiator.commands.EloCommand;
import de.diemaschine007.earogladiator.commands.SpecCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.diemaschine007.earogladiator.commands.Spawn_Command;
import de.diemaschine007.earogladiator.listener.Handle_Listener;
import de.diemaschine007.earogladiator.listener.HotBarItemInteracts;
//import de.diemaschine007.earogladiator.listener.FightDeathListener;
//import de.diemaschine007.earogladiator.listener.Listener_Kits;
import de.diemaschine007.earogladiator.methods.SetScoreboard_METHODS;

public class Main extends JavaPlugin {
	
	public static final String PREFIX = "§8[§2Practice§8] §7";
	public static final String WORLD_NAME = "practice";
	public static Main plugin;
	
	/*public static ArrayList<String> lobby = new ArrayList<String>();
	
	public static ArrayList<String> infeastqueue = new ArrayList<String>();	
	public static ArrayList<String> inbuilduhcqueue = new ArrayList<String>();
	public static ArrayList<String> innodebuffqueue = new ArrayList<String>();
	public static ArrayList<String> ingapplequeue = new ArrayList<String>();
	public static ArrayList<String> inearlyhgqueue = new ArrayList<String>();
	public static ArrayList<String> insgqueue = new ArrayList<String>();
	public static ArrayList<String> indebuffqueue = new ArrayList<String>();
	public static ArrayList<String> insoupwarsqueue = new ArrayList<String>();
	public static ArrayList<String> inarcherqueue = new ArrayList<String>();
	public static ArrayList<String> insumoqueue = new ArrayList<String>();
	public static ArrayList<String> into100queue = new ArrayList<String>();
	public static ArrayList<String> into100speedqueue = new ArrayList<String>();
	
	public static ArrayList<String> infeastfight = new ArrayList<String>();
	public static ArrayList<String> inbuilduhcfight = new ArrayList<String>();
	public static ArrayList<String> innodebufffight = new ArrayList<String>();
	public static ArrayList<String> ingapplefight = new ArrayList<String>();
	public static ArrayList<String> inearlyhgfight = new ArrayList<String>();
	public static ArrayList<String> insgfight = new ArrayList<String>();
	public static ArrayList<String> indebufffight = new ArrayList<String>();
	public static ArrayList<String> insoupwarsfight = new ArrayList<String>();
	public static ArrayList<String> inarcherfight = new ArrayList<String>();
	public static ArrayList<String> insumofight = new ArrayList<String>();
	public static ArrayList<String> into100fight = new ArrayList<String>();
	public static ArrayList<String> into100speedfight = new ArrayList<String>();*/
	
	public SetScoreboard_METHODS setScoreboard_METHODS;
	private static Location locLobbySpawn = new Location(Bukkit.getWorld("practice"), 33.5, 28.5, 22.5);




	public static Location getLocLobbySpawn() {
		return locLobbySpawn;
	}
	public void onEnable() {
		
		plugin = this;


		ArenaManager.initQueues();
		Arenas.initArenas();
		System.out.println("[Practice] Arenen initialisiert");
		getCommand("spawn").setExecutor(new Spawn_Command());
		getCommand("spec").setExecutor(new SpecCommand());
		getCommand("elo").setExecutor(new EloCommand());
		
		this.setScoreboard_METHODS = new SetScoreboard_METHODS();
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Handle_Listener(), this);
		pm.registerEvents(new HotBarItemInteracts(), this);
		pm.registerEvents(new ArenaManager(), this);
	}
	
	public static Main getPlugin() {
		return plugin;
	}

}

