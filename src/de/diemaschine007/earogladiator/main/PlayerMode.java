package de.diemaschine007.earogladiator.main;

import org.bukkit.entity.Player;

import java.util.HashMap;

public enum PlayerMode {

    FIGHT_MODE,
    LOBBY_MODE,
    SPECTATOR_MODE;

    public static HashMap<Player, PlayerMode> playermode = new java.util.HashMap<>();

    PlayerMode() {

    }

    public static boolean isInLobbyMode(Player player) {
        return (playermode.get(player) == LOBBY_MODE);
    }

    public static boolean isInSpectatorMode(Player player) {
        return (playermode.get(player) == SPECTATOR_MODE);
    }

    public static boolean isInFightMode(Player player) {
        return (playermode.get(player) == FIGHT_MODE);
    }

    public static void setPlayerMode(Player player, PlayerMode playerMode) {
        playermode.put(player, playerMode);
        player.sendMessage("DEBUG: Du bist nun im §c§l " + playerMode.name());
    }
}




