package de.diemaschine007.earogladiator.arenasystem;

import de.diemaschine007.earogladiator.methods.SetScoreboard_METHODS;
import de.diemaschine007.earogladiator.utils.LeaveQueueItem;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Queue {
    private Player player1 = null;
    private Player player2 = null;
    private boolean ranked;
    private boolean isFull = false;
    private Kit kit;
    private static HashMap<Player, Queue> playerQueueHashMap = new HashMap<>();


    public static void removePlayerFromQueue(Player p) {
        Queue queue = Queue.playerQueueHashMap.get(p);
        if (queue != null) {
            queue.removePlayer(p);
        }
    }

    public Queue(boolean ranked, Kit kit) {
        this.ranked = ranked;
        this.kit = kit;
    }


    public Player getPlayer1() {
        return player1;
    }

    public void addPlayer(Player player) {
        if (player != null) {



            if (!isFull) {
                if (player1 == null) {
                    player1 = player;
                    playerQueueHashMap.put(player, this);
                } else {
                    player2 = player;
                    playerQueueHashMap.put(player, this);
                }
                if (player1 != null &&player2 != null) {
                    isFull = true;
                }
                if (isRanked()) {
                    SetScoreboard_METHODS.RankedQueuePlayers++;
                } else {
                    SetScoreboard_METHODS.UnRankedQueuePlayers++;
                }

                LeaveQueueItem.run(player);
            }
        }

    }

    public void clear() {
        playerQueueHashMap.remove(player1);
        playerQueueHashMap.remove(player2);
        player1 = null;
        player2 = null;
        if (isRanked()) {
            SetScoreboard_METHODS.RankedQueuePlayers--;
            SetScoreboard_METHODS.RankedQueuePlayers--;
        } else {
            SetScoreboard_METHODS.UnRankedQueuePlayers--;
            SetScoreboard_METHODS.UnRankedQueuePlayers--;
        }
        isFull = false;

    }

    public void removePlayer(Player player) {
        if (player != null) {
            if (player1 != null) {
                if (player.equals(player1)) {
                    player1 = player2;
                    player2 = null;
                    playerQueueHashMap.remove(player);
                }
            }
            if (player2 != null) {
                if (player.equals(player2)) {
                    player2 = null;
                    playerQueueHashMap.remove(player);
                }
            }
            this.isFull = false;
            if (isRanked()) {
                SetScoreboard_METHODS.RankedQueuePlayers--;
            } else {
                SetScoreboard_METHODS.UnRankedQueuePlayers--;
            }


        }

    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getPlayer2() {
        return player2;
    }

    public boolean isRanked() {
        return ranked;
    }

    public void setRanked(boolean ranked) {
        this.ranked = ranked;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean full) {
        isFull = full;
    }



}
