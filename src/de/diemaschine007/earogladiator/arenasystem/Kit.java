package de.diemaschine007.earogladiator.arenasystem;

import de.diemaschine007.earogladiator.utils.Items;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Kit {
    FEAST("§aFeast"),
    BUILD_UHC("§aBuildUHC"),
    NO_DEBUFF("§aNoDebuff"),
    GAPPLE("§aGapple"),
    EARLY_HG("§aEarlyHG"),
    SG("§aSG"),
    DEBUFF("§aDebuff"),
    ARCHER("§aArcher"),
    SOUPWARS("§aSoupWars1vs1"),
    SUMO("§aSumo"),
    TO100("§aTo100"),
    TO100SPEED("§aTo100-Speed");


    private String name;

    Kit(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

}
