package de.diemaschine007.earogladiator.arenasystem;

import de.diemaschine007.earogladiator.main.Main;
import de.diemaschine007.earogladiator.main.PlayerMode;
import de.diemaschine007.earogladiator.utils.Items;
import de.diemaschine007.earogladiator.utils.LobbyItems;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Manages the matching of 2 players, that want to play the same kit. When 2 players have been matched,
 * an Arena object instance gets initialized and the game starts.
 */
public class ArenaManager implements Listener {


    FileConfiguration cfg = Main.getPlugin().getConfig();

    public static int playersInQueue;
    public static int playersInFight;

    public ItemStack FeastQueueItem = Items.createItemStack(Material.MUSHROOM_SOUP, 1, 0, "§aFeast");
    public ItemStack BuildUhcQueueItem = Items.createItemStack(Material.LAVA_BUCKET, 1, 0, "§aBuildUHC");
    public ItemStack NoDebuffQueueItem = Items.createItemStack(Material.POTION, 1, 16421, "§aNoDebuff");
    public ItemStack GappleQueueItem = Items.createItemStack(Material.GOLDEN_APPLE, 1, 1, "§aGapple");
    public ItemStack EarlyHgQueueItem = Items.createItemStack(Material.STONE_SWORD, 1, 0, "§aEarlyHG");
    public ItemStack SgQueueItem = Items.createItemStack(Material.FISHING_ROD, 1, 0, "§aSG");
    public ItemStack DebuffQueueItem = Items.createItemStack(Material.POTION, 1, 16388, "§aDebuff");
    public ItemStack ArcherQueueItem = Items.createItemStack(Material.BOW, 1, 0, "§aArcher");
    public ItemStack SoupWars1v1QueueItem = Items.createItemStack(Material.BEACON, 1, 0, "§aSoupWars1vs1");
    public ItemStack SumoQueueItem = Items.createItemStack(Material.LEASH, 1, 0, "§aSumo");
    public ItemStack To100QueueItem = Items.createItemStack(Material.BEDROCK, 1, 0, "§aTo100");
    public ItemStack To100SpeedQueueItem = Items.createItemStack(Material.POTION, 1, 8226, "§aTo100-Speed");
    public ItemStack CloseInvItem = Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv");





    public static ArrayList<Player> inCoolDown = new ArrayList<>();

    public FileConfiguration getCfg() {
        return cfg;
    }

    /**
     * maps player to a kit. Mapping is done, when player has chosen a kit.
     */
    public static HashMap<Player, Arena> playerToArenaMap = new HashMap<>();
    public static HashMap<Player, Boolean> isPlayerRanked = new HashMap<>();
    public static HashMap<Player, Integer> playerEloMap = new HashMap<>();

    private HashMap<Kit, Queue> kitQueueHashMap = new HashMap<>();


    /**
     * Arenas
     */


    private static ArrayList<Queue> rankedQueues = new ArrayList<>();
    private static ArrayList<Queue> unrankedQueues = new ArrayList<>();


    private ArrayList<Queue> getQueueList(boolean ranked) {
        if(ranked) {
            return this.rankedQueues;
        } else {
            return this.unrankedQueues;
        }
    }

    public static void initQueues() {
        for(Kit kit: Kit.values()) {
            Queue q1 = new Queue(true,kit);
            Queue q2 = new Queue(false,kit);
            rankedQueues.add(kit.ordinal(),q1);
            unrankedQueues.add(kit.ordinal(),q2);
        }
    }

    //TODO: Add other arenas & queues.
    static Queue queueUnRankedFeast = new Queue(false, Kit.FEAST);
    static Queue queueRankedFeast = new Queue(true, Kit.FEAST);

    static Queue queueUnRankedBuildUHC = new Queue(false, Kit.BUILD_UHC);
    static Queue queueRankedBuildUHC = new Queue(true, Kit.BUILD_UHC);

    static Queue queueUnRankedNoDebuff = new Queue(false, Kit.NO_DEBUFF);
    static Queue queueRankedNoDebuff = new Queue(true, Kit.NO_DEBUFF);

    static Queue queueUnRankedGapple = new Queue(false, Kit.GAPPLE);
    static Queue queueRankedGapple = new Queue( true, Kit.GAPPLE);

    static Queue queueUnRankedEarlyHg = new Queue(false, Kit.EARLY_HG);
    static Queue queueRankedEarlyHg = new Queue(true, Kit.EARLY_HG);

    static Queue queueUnRankedSg = new Queue( false, Kit.SG);
    static Queue queueRankedSg = new Queue(true, Kit.SG);

    static Queue queueUnRankedDebuff = new Queue(false, Kit.DEBUFF);
    static Queue queueRankedDebuff = new Queue( true, Kit.DEBUFF);

    static Queue queueUnRankedArcher = new Queue(false, Kit.ARCHER);
    static Queue queueRankedArcher = new Queue(true, Kit.ARCHER);

    static Queue queueUnRankedSoupWars = new Queue(false, Kit.SOUPWARS);
    static Queue queueRankedSoupWars = new Queue(true, Kit.SOUPWARS);

    static Queue queueUnRankedSumo = new Queue(false, Kit.SUMO);
    static Queue queueRankedSumo = new Queue(true, Kit.SUMO);

    static Queue queueUnRankedTo100 = new Queue(false, Kit.TO100);
    static Queue queueRankedTo100 = new Queue(true, Kit.TO100);

    static Queue queueUnRankedTo100Speed = new Queue(false, Kit.TO100SPEED);
    static Queue queueRankedTo100Speed = new Queue(true, Kit.TO100SPEED);








    @EventHandler
    public void onPlayerJoinQueue(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        ItemStack currentItemStack = event.getCurrentItem();
        if (PlayerMode.isInLobbyMode(player)) {
            if (event.getClickedInventory() != null) {
                if (currentItemStack.equals(CloseInvItem)) {
                    player.closeInventory();
                }
                if (event.getInventory().getName().equalsIgnoreCase("§9Ranked-Queue")) {
                    isPlayerRanked.put(player, true);
                    player.closeInventory();
                }
                if (event.getInventory().getName().equalsIgnoreCase("§2Unranked-Queue")) {
                    isPlayerRanked.put(player, false);
                    player.closeInventory();
                }
                //KITS

                if (event.getCurrentItem().equals(FeastQueueItem)) {
                    Queue queue = this.getQueueList(isPlayerRanked.get(player)).get(Kit.FEAST.ordinal());
                    try2JoinArena(queue,player);
                }
                if (event.getCurrentItem().equals(NoDebuffQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedNoDebuff, player);
                    } else {
                        try2JoinArena(queueUnRankedNoDebuff, player);
                    }
                }
                if (event.getCurrentItem().equals(GappleQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedGapple, player);
                    } else {
                        try2JoinArena(queueUnRankedGapple, player);
                    }
                }
                if (event.getCurrentItem().equals(BuildUhcQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedBuildUHC, player);
                    } else {
                        try2JoinArena(queueUnRankedBuildUHC, player);
                    }
                }
                if (event.getCurrentItem().equals(EarlyHgQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedEarlyHg, player);
                    } else {
                        try2JoinArena(queueUnRankedEarlyHg, player);
                    }
                }
                if (event.getCurrentItem().equals(SgQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedSg, player);
                    } else {
                        try2JoinArena(queueUnRankedSg, player);
                    }
                }
                if (event.getCurrentItem().equals(DebuffQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedDebuff, player);
                    } else {
                        try2JoinArena(queueUnRankedDebuff, player);
                    }
                }
                if (event.getCurrentItem().equals(ArcherQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedArcher, player);
                    } else {
                        try2JoinArena(queueUnRankedArcher, player);
                    }
                }
                if (event.getCurrentItem().equals(SoupWars1v1QueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedSoupWars, player);
                    } else {
                        try2JoinArena(queueUnRankedSoupWars, player);
                    }
                }
                if (event.getCurrentItem().equals(SumoQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedSumo, player);
                    } else {
                        try2JoinArena(queueUnRankedSumo, player);
                    }
                }
                if (event.getCurrentItem().equals(To100QueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedTo100, player);
                    } else {
                        try2JoinArena(queueUnRankedTo100, player);
                    }
                }
                if (event.getCurrentItem().equals(To100SpeedQueueItem)) {
                    if (isPlayerRanked.get(player)) {
                        try2JoinArena(queueRankedTo100Speed, player);
                    } else {
                        try2JoinArena(queueUnRankedTo100Speed, player);
                    }
                }

            }
        }

    }

    //There are 3 types of arenas. The types differ from the

    public void try2JoinArena(Queue queue, Player player) {

        if (!queue.isFull()) {
            queue.addPlayer(player);
            player.sendMessage(Main.PREFIX + "Du bist der Queue beigetreten");
        }
        if (queue.isFull()) {

            if (queue.getKit() != Kit.SUMO && queue.getKit() != Kit.SOUPWARS) {

                for (int i = 0; i < Arenas.getStandardArenas().size(); i++) {
                    Arena arena = Arenas.getStandardArenas().get(i);
                    if (!arena.isFull()) {

                        arena.setPlayers(queue.getPlayer1(), queue.getPlayer2());
                        queue.clear();
                        arena.setKit(queue.getKit());
                        arena.startGame();

                        break;
                    } else {
                        player.sendMessage(Main.PREFIX + "Keine Arena frei.");

                    }
                }

            }
            if (queue.getKit() == Kit.SUMO) {
                for (Arena arenas : Arenas.getSumoArenas()) {
                    if (!arenas.isFull()) {
                        arenas.setPlayers(queue.getPlayer1(), queue.getPlayer2());
                        queue.clear();
                        arenas.setKit(queue.getKit());
                        arenas.startGame();

                        break;
                    } else {
                        player.sendMessage(Main.PREFIX + "Keine Arena frei.");
                    }
                }
            }
            if (queue.getKit() == Kit.SOUPWARS) {

                for (Arena arenas : Arenas.getSoupWarsArenas()) {
                    if (!arenas.isFull()) {
                        arenas.setPlayers(queue.getPlayer1(), queue.getPlayer2());

                        arenas.setKit(queue.getKit());
                        arenas.startGame();
                        queue.clear();
                        break;
                    } else {
                        player.sendMessage(Main.PREFIX + "Keine Arena frei.");
                    }

                }
            }

        }
    }

    @EventHandler
    public void onPlayerQuitGame(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (PlayerMode.isInFightMode(player)) {
            if (playerToArenaMap.get(player) != null) {
                Arena arena = playerToArenaMap.get(player);
                Player player2 = arena.otherPlayer(player);
                player2.sendMessage(Main.PREFIX + "Der Spieler " + player.getName() + " hat die Flucht ergriffen!");
                arena.endGame(arena.otherPlayer(player));

            }
        }
        if (PlayerMode.isInLobbyMode(player)) {
            Queue.removePlayerFromQueue(player);

            isPlayerRanked.remove(player);
        }

    }




    public static Arena getArenaFromPlayer(Player player) {
        return playerToArenaMap.get(player);
    }


    @EventHandler
    public void LeaveQueueItemInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.getItemInHand() != null) {
            if (PlayerMode.isInLobbyMode(player)) {
                if (player.getItemInHand().getType() == Material.REDSTONE) {
                    if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lLeave Queue")) {
                            Queue.removePlayerFromQueue(player);
                            isPlayerRanked.remove(player);
                            event.setCancelled(true);
                            player.setFireTicks(0);
                            player.setHealth(20D);
                            player.getInventory().clear();
                            player.getInventory().setArmorContents(null);
                            LobbyItems.run(player);
                            player.sendMessage(Main.PREFIX + "§7Du hast die §aQueue §7verlassen.");
                        }
                    }
                }
            }
        }
    }

    /**
     * EVENT CANCELING
     */
    @EventHandler
    public void onPlayerBreakBlocks(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        Arena arena = playerToArenaMap.get(player);
        if (arena != null) {
            Kit kit = arena.getKit();
            if (PlayerMode.isInFightMode(player)) {
                if (kit != Kit.BUILD_UHC) {
                    event.setCancelled(true);
                }
                if (!isBlockWhitelisted(block.getType()) ) {
                    event.setCancelled(true);
                }
            }
            if (PlayerMode.isInLobbyMode(player)) {
                if (player.getGameMode() != GameMode.CREATIVE) {
                    event.setCancelled(true);
                }
            }

        }

    }
    public static boolean isBlockWhitelisted(Material material) {
        if (material.equals(Material.COBBLESTONE) || material.equals(Material.WOOD)) {
            return true;
        } else {
            return false;
        }
    }
    @EventHandler
    public void onPlayerDropItems(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        Arena arena = playerToArenaMap.get(player);
        if (arena != null) {
            Kit kit = playerToArenaMap.get(player).getKit();
            if (kit.equals(Kit.ARCHER) || kit.equals(Kit.BUILD_UHC) || kit.equals(Kit.TO100) || kit.equals(Kit.TO100SPEED)) {
                event.setCancelled(true);
            }
        }

    }
    @EventHandler
    public void onPlayerFoodLevel(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            Arena arena = playerToArenaMap.get(player);
            if (arena != null) {
                Kit kit = arena.getKit();
                if (!kit.equals(Kit.BUILD_UHC) || kit.equals(Kit.SG) || kit.equals(Kit.SOUPWARS)) {
                    event.setCancelled(true);
                }
            }

        }
    }
    @EventHandler
    public void onPlayerHitPlayer(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            Arena arena = playerToArenaMap.get(player);
            if (arena != null) {
                Kit kit = arena.getKit();
                if (kit.equals(Kit.TO100) || kit.equals(Kit.TO100SPEED)) {
                    event.setDamage(0);
                    if (player.getLevel() < 99) {
                        player.setLevel(player.getLevel() + 1);
                    } else {
                        arena.endGame(player);
                    }
                }
            }
        }
    }
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {

        Player player = event.getPlayer();
        Arena arena = playerToArenaMap.get(player);
        if (arena != null) {
            Kit kit = arena.getKit();
            if (inCoolDown.contains(player)) {
                event.setCancelled(true);
            }
            if (kit == Kit.SUMO) {
                if (event.getTo().getY() < 3) {
                    player.damage(20);
                }
            }
        }
    }
}
