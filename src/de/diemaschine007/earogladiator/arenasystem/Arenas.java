package de.diemaschine007.earogladiator.arenasystem;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class Arenas {

    private static ArrayList<Arena> standardArenas = new ArrayList<>();
    private static ArrayList<Arena> sumoArenas = new ArrayList<>();
    private static ArrayList<Arena> soupWarsArenas = new ArrayList<>();


    public static ArrayList<Arena> getSoupWarsArenas() {
        return soupWarsArenas;
    }

    public static ArrayList<Arena> getStandardArenas() {
        return standardArenas;
    }

    public static ArrayList<Arena> getSumoArenas() {
        return sumoArenas;
    }


    static void createArenas(int amount, ArenaType arenaType, ArrayList arrayList) {
        for (int i = 0; i < amount; i++) {
            arrayList.add(new Arena(arenaType));
        }
    }



    public static void initArenas() {
        createArenas(1, ArenaType.STONE, standardArenas);
        //createArenas(6, ArenaType.HEART, standardArenas);


        // createArenas(6, ArenaType.SUMO, sumoArenas);
        // createArenas(6, ArenaType.SOUP_WARS, soupWarsArenas);
    }





    static Location arena1Loc = new Location(Bukkit.getWorld("practice"), 1000, 100, 1000);
    static Vector arenaType1spawnVec1 = new Vector(-48, 0, 0);
    static Vector arenaType1spawnVec2 = new Vector(48, 0, 0);

    static Vector arenaType1edgeVec1 = new Vector(-50, 0, -30);
    static Vector arenaType1edgeVec2 = new Vector(50, 49, 30);


    static Vector arenaType2spawnVec1 = new Vector(-36, 0, 0);
    static Vector arenaType2spawnVec2 = new Vector(36, 0, 0);

    static Vector arenaType2edgeVec1 = new Vector(-42, 0, -38);
    static Vector arenaType2edgeVec2 = new Vector(42, 57, 38);


    static Vector arenaType3spawnVec1 = new Vector(-36, 0, 0);
    static Vector arenaType3spawnVec2 = new Vector(36, 0, 0);

    static Vector arenaType3edgeVec1 = new Vector(-43, 0, -38);
    static Vector arenaType3edgeVec2 = new Vector(43, 57, 38);

    static Vector arenaSumoSpawnVec1 = new Vector(0, 0, 5);
    static Vector arenaSumoSpawnVec2 = new Vector(0, 0, -5);





    /*
    //Stone Arenas

    static Location arena1baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 1000, 100, 1000);
    static Arena arena1 = new Arena(arena1baseLoc, arenaType1spawnVec1, arenaType1spawnVec2, arenaType1edgeVec1, arenaType1edgeVec2);

    static Location arena2baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 1500, 100, 1500);
    static Arena arena2 = new Arena(arena2baseLoc, arenaType1spawnVec1, arenaType1spawnVec2, arenaType1edgeVec1, arenaType1edgeVec2);

    static Location arena3baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 2000, 100, 2000);
    static Arena arena3 = new Arena(arena3baseLoc, arenaType1spawnVec1, arenaType1spawnVec2, arenaType1edgeVec1, arenaType1edgeVec2);

    //Heart Arenas

    static Location arena4baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 2500, 100, 2500);
    static Arena arena4 = new Arena(arena4baseLoc, arenaType2spawnVec1, arenaType2spawnVec2, arenaType2edgeVec1, arenaType2edgeVec2);
    static Location arena5baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 3000, 100, 3000);
    static Arena arena5 = new Arena(arena5baseLoc, arenaType2spawnVec1, arenaType2spawnVec2, arenaType2edgeVec1, arenaType2edgeVec2);
    static Location arena6baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 3500, 100, 3500);
    static Arena arena6 = new Arena(arena6baseLoc, arenaType2spawnVec1, arenaType2spawnVec2, arenaType2edgeVec1, arenaType2edgeVec2);

    //Mushroom Arenas

    static Location arena7baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 4000, 100, 4000);
    static Arena arena7 = new Arena(arena7baseLoc, arenaType3spawnVec1, arenaType3spawnVec2, arenaType3edgeVec1, arenaType3edgeVec2);
    static Location arena8baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 4500, 100, 4500);
    static Arena arena8 = new Arena(arena8baseLoc, arenaType3spawnVec1, arenaType3spawnVec2, arenaType3edgeVec1, arenaType3edgeVec2);
    static Location arena9baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 5000, 100, 5000);
    static Arena arena9 = new Arena(arena9baseLoc, arenaType3spawnVec1, arenaType3spawnVec2, arenaType3edgeVec1, arenaType3edgeVec2);

    //Sumo Arenas

    static Location arena10baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 5500, 3, 5500);
    static Arena arena10 = new Arena(arena10baseLoc, arenaSumoSpawnVec1, arenaSumoSpawnVec2, null, null);
    static Location arena11baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 6000, 3, 6000);
    static Arena arena11 = new Arena(arena11baseLoc, arenaSumoSpawnVec1, arenaSumoSpawnVec2, null, null);
    static Location arena12baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 5500, 3, 6000);
    static Arena arena12 = new Arena(arena12baseLoc, arenaSumoSpawnVec1, arenaSumoSpawnVec2, null, null);
    static Location arena13baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 6000, 3, 5500);
    static Arena arena13 = new Arena(arena13baseLoc, arenaSumoSpawnVec1, arenaSumoSpawnVec2, null, null);
    static Location arena14baseLoc = new Location(Bukkit.getWorld(Main.WORLD_NAME), 6500, 3, 6500);
    static Arena arena14 = new Arena(arena14baseLoc, arenaSumoSpawnVec1, arenaSumoSpawnVec2, null, null);
    public static void assignArenasToArenaList() {
        standardArenas.add(arena1);
        standardArenas.add(arena2);
        standardArenas.add(arena3);
        standardArenas.add(arena4);
        standardArenas.add(arena5);
        standardArenas.add(arena6);
        standardArenas.add(arena7);
        standardArenas.add(arena8);
        standardArenas.add(arena9);


        sumoArenas.add(arena10);
        sumoArenas.add(arena11);
        sumoArenas.add(Arenas.arena12);
        sumoArenas.add(Arenas.arena13);
        sumoArenas.add(Arenas.arena14);
    }



*/
}
