package de.diemaschine007.earogladiator.arenasystem;

import de.diemaschine007.earogladiator.main.Main;
import de.diemaschine007.earogladiator.main.PlayerMode;
import de.diemaschine007.earogladiator.methods.SetScoreboard_METHODS;
import de.diemaschine007.earogladiator.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Arena always has two players assigned or it has no players, means: is empty.
 */
public class Arena {
    private int taskID;

    private static Location baseLocation = new Location(Bukkit.getWorld("practice"), 500, 100, 500);

    private Location spawnLocation1 = new Location(Bukkit.getWorld(Main.WORLD_NAME), 0, 0, 0);
    private Location spawnLocation2 = new Location(Bukkit.getWorld(Main.WORLD_NAME), 0, 0, 0);

    private Location edgeLocation1 = new Location(Bukkit.getWorld(Main.WORLD_NAME), 0, 0, 0);
    private Location edgeLocation2 = new Location(Bukkit.getWorld(Main.WORLD_NAME), 0, 0, 0);

    private Player player1;
    private Player player2;

    private Player winner = null;
    private Player loser = null;

    private ArenaType arenaType;




    public void setKit(Kit kit) {
        this.kit = kit;
    }

    private Kit kit;
    private static Location locLobbySpawn = new Location(Bukkit.getWorld("practice"), 33.5, 28.5, 22.5);
    private HashMap<Player, Inventory> playerInventoryHashMap = new HashMap<>();

    private static int numOfArenas;

    private boolean isFull = false;
    private int generatedElo;

    public Arena(ArenaType arenaType) {

        this.arenaType = arenaType;

        this.baseLocation.add(new Vector(500, 0, 500));

        this.spawnLocation1.setX(baseLocation.getBlockX());
        this.spawnLocation1.setY(baseLocation.getBlockY() + 1);
        this.spawnLocation1.setZ(baseLocation.getBlockZ());
        spawnLocation1.add(arenaType.getSpawnVector1());

        this.spawnLocation2.setX(baseLocation.getBlockX());
        this.spawnLocation2.setY(baseLocation.getBlockY() + 1);
        this.spawnLocation2.setZ(baseLocation.getBlockZ());
        spawnLocation2.add(arenaType.getSpawnVector2());


        if (arenaType.getEdgeVector1() != null|| arenaType.getEdgeVector2() != null) {
            this.edgeLocation1.setX(baseLocation.getX());
            this.edgeLocation1.setY(baseLocation.getY());
            this.edgeLocation1.setZ(baseLocation.getZ());
            edgeLocation1.add(arenaType.getEdgeVector1());

            this.edgeLocation2.setX(baseLocation.getX());
            this.edgeLocation2.setY(baseLocation.getY());
            this.edgeLocation2.setZ(baseLocation.getZ());
            edgeLocation2.add(arenaType.getEdgeVector2());
        }

        numOfArenas++;
    }

    public void startGame() {
        if(this.isFull) {

            player1.sendMessage("SpawnLoc1: "+ spawnLocation1.getBlockX() + " " + spawnLocation1.getY() + " " + spawnLocation1.getBlockZ() );

            player1.getInventory().clear();
            player2.getInventory().clear();

            if (kit == Kit.FEAST) {
                Feast.run(player1);
                Feast.run(player2);
            }
            if (kit == Kit.BUILD_UHC) {
                BuildUHC.run(player1);
                BuildUHC.run(player2);
            }
            if (kit == Kit.NO_DEBUFF) {
                NoDebuff.run(player1);
                NoDebuff.run(player2);
            }
            if (kit == Kit.GAPPLE) {
                Gapple.run(player1);
                Gapple.run(player2);
            }
            if (kit == Kit.EARLY_HG) {
                EarlyHG.run(player1);
                EarlyHG.run(player2);
            }
            if (kit == Kit.SG) {
                SG.run(player1);
                SG.run(player1);
            }
            if (kit == Kit.DEBUFF) {
                Debuff.run(player1);
                Debuff.run(player2);
            }
            if (kit == Kit.ARCHER) {
                Archer.run(player1);
                Archer.run(player2);
            }
            if (kit == Kit.SOUPWARS) {
                //SoupWars1v1.run(player1)
                //SoupWars1v1.run(player2)
            }
            if (kit == Kit.SUMO) {
                ArenaManager.inCoolDown.add(player1);
                ArenaManager.inCoolDown.add(player2);

                taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {

                    int count = 3;

                    @Override
                    public void run() {
                        player1.sendMessage("§e" + count);
                        player2.sendMessage("§e" + count);


                        count--;
                        if (count == 0) {
                            player1.sendMessage("§e§lSTART");
                            player2.sendMessage("§e§lSTART");
                            ArenaManager.inCoolDown.remove(player1);
                            ArenaManager.inCoolDown.remove(player2);
                            Bukkit.getScheduler().cancelTask(taskID);
                        }
                    }
                }, 0, 3*20);
            }


            if (kit == Kit.TO100 || kit == Kit.TO100SPEED) {
                To100.run(player1);
                To100.run(player2);
            }




            SetScoreboard_METHODS.FightPlayers++;
            SetScoreboard_METHODS.FightPlayers++;

            player1.teleport(spawnLocation1);
            player2.teleport(spawnLocation2);

            player1.getLocation().setYaw(spawnLocation1.getYaw());
            player2.getLocation().setYaw(spawnLocation2.getYaw());

            player1.getLocation().setPitch(spawnLocation1.getPitch());
            player2.getLocation().setPitch(spawnLocation2.getPitch());


            player1.setHealth(20D);
            player2.setHealth(20D);
            player1.setFireTicks(0);
            player2.setFireTicks(0);

            PlayerMode.setPlayerMode(player1, PlayerMode.FIGHT_MODE);
            PlayerMode.setPlayerMode(player2, PlayerMode.FIGHT_MODE);

            ArenaManager.playerToArenaMap.put(player1, this);
            ArenaManager.playerToArenaMap.put(player2, this);

        }
    }

    public void clear() {
        player1 = null;
        player2 = null;
        this.isFull = false;
        if (kit.equals(Kit.BUILD_UHC) || kit.equals(Kit.SOUPWARS)) {
            int x1 = edgeLocation1.getBlockX();
            int y1 = edgeLocation1.getBlockY();
            int z1 = edgeLocation1.getBlockZ();
            int x2 = edgeLocation2.getBlockX();
            int y2 = edgeLocation2.getBlockY();
            int z2 = edgeLocation2.getBlockZ();

            int minX = Integer.min(x1, x2);
            int minY = Integer.min(y1, y2);
            int minZ = Integer.min(z1, z2);

            int maxX = Integer.max(x1, x2);
            int maxY = Integer.max(y1, y2);
            int maxZ = Integer.max(z1, z2);

            for (int x = minX; x < maxX + 1; x++) {
                for (int y = minY; y < maxY + 1; y++) {
                    for (int z = minZ; z < maxZ + 1; z++) {
                        //if (ArenaManager.isBlockWhitelisted(Bukkit.getWorld("practice").getBlockAt(x, y, z).getType())) {
                        Bukkit.getWorld("practice").getBlockAt(x, y, z).setType(Material.AIR);
                        //}
                    }
                }
            } System.out.println("[Practice] Arena gecleart!");
        }

    }

    public void setPlayers(Player player1, Player player2) {
        if (player1 != null && player2 != null) {
            this.player1 = player1;
            this.player2 = player2;
            this.isFull = true;
            ArenaManager.playerToArenaMap.put(player1, this);
            ArenaManager.playerToArenaMap.put(player2, this);
        }
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public boolean isFull() {
        return isFull;
    }


    public Kit getKit() {
        return kit;
    }

    public ArenaType getArenaType() {
        return arenaType;
    }

    public void endGame(Player winner) {
        this.winner = winner;
        if (winner != null) {

            this.loser = otherPlayer(winner);
            double winnerHealth = winner.getHealth();

            winner.sendMessage(Main.PREFIX + "Du hast gegen §c" + loser.getName() + "§7 gewonnen");

            putPlayerToLobby(player1);
            putPlayerToLobby(player2);
            manageElo();

            //winner.openInventory(playerInventoryHashMap.get(loser));
            //loser.openInventory(playerInventoryHashMap.get(winner));
            if (kit == Kit.TO100 || kit == Kit.TO100SPEED) {
                loser.sendMessage(Main.PREFIX + "§c" + winner.getName() + " §7hat die §a100 §7Hits vor dir erreicht");
                loser.sendMessage(Main.PREFIX+ "Du hattest erst§a " + loser.getLevel() + " Hits.");
                winner.sendMessage(Main.PREFIX+"§c§7"+loser.getName()+" hatte erst §a"+loser.getLevel()+" §7Hits.");
                winner.setLevel(0);

            } else {
                loser.sendMessage(Main.PREFIX + "Du hast gegen §c" + winner.getName() + " "+ winnerHealth + " §7 verloren.");
            }


            SetScoreboard_METHODS.FightPlayers--;
            SetScoreboard_METHODS.FightPlayers--;
        }


//DEBUG
        System.out.println("[Practice] Arena wird gecleart");
        clear();
    }

    private int generateEloForWinner(Player winner) {
        Player killedPlayer = this.otherPlayer(winner);
        if (winner != null) {
            int eloDifference = ArenaManager.playerEloMap.get(winner) - ArenaManager.playerEloMap.get(killedPlayer);

            if (eloDifference > 0) {
                generatedElo = eloDifference;
            } else {
                if (eloDifference <= 0) {
                    generatedElo = 20;
                }
            }


            return generatedElo;
        } else {
            return 0;
        }

    }

    private void manageElo() {
        if (ArenaManager.isPlayerRanked.get(winner)) {
            FileConfiguration cfg = Main.getPlugin().getConfig();
            int winnerElo = ArenaManager.playerEloMap.get(winner);
            int loserElo = ArenaManager.playerEloMap.get(loser);

            winnerElo = generateEloForWinner(winner) + winnerElo;
            loserElo = loserElo - generateEloForWinner(winner);

            cfg.set("elo." + winner.getUniqueId(), winnerElo);
            cfg.set("elo." + loser.getUniqueId(), loserElo);
            Main.getPlugin().saveConfig();

            if (loser != null) {
                loser.sendMessage(Main.PREFIX + "Dein Elo ist§6 " + loserElo);
            }

            winner.sendMessage(Main.PREFIX + "Dein Elo ist§6 " + winnerElo);

            ArenaManager.playerEloMap.put(winner, winnerElo);
            ArenaManager.playerEloMap.put(loser, loserElo);

        }
    }

    private void putPlayerToLobby(Player player) {
        if (!PlayerMode.isInLobbyMode(player)) {
            if (player == winner) {
                player.teleport(locLobbySpawn);
                LobbyItems.run(player);

                player.setFoodLevel(20);
                player.setHealth(20);
                for (PotionEffect effect : player.getActivePotionEffects()) {
                    player.removePotionEffect(effect.getType());
                }
            }
            PlayerMode.setPlayerMode(player, PlayerMode.LOBBY_MODE);
            ArenaManager.playerToArenaMap.remove(player1);
        }
    }

    public Player otherPlayer(Player player) {
        if (player.equals(player1)) {
            return player2;
        }
        if (player.equals(player2)) {
            return player1;
        } else {
            return null;
        }
    }


    public void endGameShowWinnerInventory(Player loser, Player winner) {
        HashMap<Player, Inventory> winnerInventoryMap = new HashMap<>();
        winnerInventoryMap.put(winner, winner.getInventory());
        //loser.sendmessage with inventory click text
    }

    private void saveInventory(Player player) {
        Inventory openingInventory = Bukkit.createInventory(null, 9*6, "§cInventar von");
        if (player != null) {

            for (int i = 9; i < 35; i++)  {
                openingInventory.setItem(i-8, player.getInventory().getItem(i));
            }
            for (int i = 0; i < 8; i++) {
                openingInventory.setItem(i+27, player.getInventory().getItem(i));
            }
            ItemStack healthItem = Items.createItemStack(Material.SPECKLED_MELON, 1, 0, "§4§lHealth");
            ArrayList<String> lore = new ArrayList<>();
            lore.add("§4"+ player.getHealth());
            healthItem.getItemMeta().setLore(lore);

            openingInventory.setItem(45, healthItem);

            ItemStack foodItem = Items.createItemStack(Material.COOKED_BEEF, 1, 0, "§6§lHunger");
            lore.add("§4"+ player.getFoodLevel());
            healthItem.getItemMeta().setLore(lore);

            openingInventory.setItem(46, foodItem);

            if (kit == Kit.FEAST || kit == Kit.EARLY_HG) {
                int leftOverSoups = 0;
                for (int i = player.getInventory().getSize(); i < player.getInventory().getSize(); i++) {

                    if (player.getInventory().getItem(i).getType() == Material.MUSHROOM_SOUP) {
                        leftOverSoups++;
                    }
                }
                ItemStack soupItem = Items.createItemStack(Material.MUSHROOM_SOUP, 1, 0, "§6§lHunger");
                lore.add("§4"+ leftOverSoups);
                soupItem.getItemMeta().setLore(lore);
                openingInventory.setItem(47, soupItem);
            } else {
                if (kit == Kit.NO_DEBUFF || kit == Kit.DEBUFF) {
                    int leftOverPots = 0;
                    for (int i = player.getInventory().getSize(); i < player.getInventory().getSize(); i++) {

                        if (player.getInventory().getItem(i).equals(new ItemStack(Material.POTION, 1, (short) 16421))) {
                            leftOverPots++;
                        }
                    }
                    ItemStack potItem = Items.createItemStack(Material.POTION, 1, 16421, "§6§lPots");
                    lore.add("§4"+ leftOverPots);
                    potItem.getItemMeta().setLore(lore);
                    openingInventory.setItem(47, potItem);
                }
            }

            playerInventoryHashMap.put(player, openingInventory);
        }
    }

}
