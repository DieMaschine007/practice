package de.diemaschine007.earogladiator.arenasystem;

import org.bukkit.util.Vector;

public enum ArenaType {
    /**
     * Is used to distinguish between different arena coordinate types
     *
     * spawnVector is used to calculate the spawn position of the players
     *
     * edgeVector is used to calculate the edge positions for clearing the arena from blocks and items.
     */
    STONE(new Vector(-48, 0, 0), new Vector(48, 0, 0), new Vector(50, 22, 30), new Vector(-50, 0, -30)),
    HEART(new Vector(-36, 0, 0), new Vector(36, 0, 0), new Vector(-42, 0, -38), new Vector(42, 57, 38)),
    MUSHROOM(new Vector(-36, 0, 0), new Vector(36, 0, 0), new Vector(-43, 0, -38), new Vector(43, 57, 38)),
    SOUP_WARS(new Vector(), new Vector(), new Vector(), new Vector()),
    SUMO(new Vector(0, 0, 5), new Vector(0, 0, -5));



    private Vector spawnVector1;
    private Vector spawnVector2;

    private Vector edgeVector1;
    private Vector edgeVector2;




    ArenaType(Vector spawnVector1, Vector spawnVector2, Vector edgeVector1, Vector edgeVector2) {
        this.spawnVector1 = spawnVector1;
        this.spawnVector2 = spawnVector2;

        this.edgeVector1 = edgeVector1;
        this.edgeVector2 = edgeVector2;
    }

    ArenaType(Vector spawnVector1, Vector spawnVector2) {
        this.spawnVector1 = spawnVector1;
        this.spawnVector2 = spawnVector2;
    }



    public Vector getEdgeVector1() {
        return edgeVector1;
    }

    public Vector getEdgeVector2() {
        return edgeVector2;
    }

    public Vector getSpawnVector1() {
        return spawnVector1;
    }

    public Vector getSpawnVector2() {
        return spawnVector2;
    }

}
