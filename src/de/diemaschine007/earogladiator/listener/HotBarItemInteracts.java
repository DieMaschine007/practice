package de.diemaschine007.earogladiator.listener;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.diemaschine007.earogladiator.main.PlayerMode;
import de.diemaschine007.earogladiator.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import de.diemaschine007.earogladiator.main.Main;

public class HotBarItemInteracts implements Listener {
	
	@EventHandler
	public void onInteract3(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(PlayerMode.isInLobbyMode(p)) {
	    if (p.getItemInHand().getType() == Material.SLIME_BALL) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
			if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lLeave")) {
				p.sendMessage(Main.PREFIX + "§7Du wurdest auf die §4Lobby §7verschoben.");
				ByteArrayOutputStream b = new ByteArrayOutputStream();
				DataOutputStream out = new DataOutputStream(b);

				try {
					out.writeUTF("Connect");
				} catch (IOException e1) {
					p.sendMessage(Main.PREFIX + "§cThis Server is currently offline!");
				}
				try {
					out.writeUTF("lobby-01");
				} catch (IOException e1) {
					p.sendMessage(Main.PREFIX + "§cThis Server is currently offline!");
				}
					p.sendPluginMessage(Main.getPlugin(), "BungeeCord", b.toByteArray());
					}
				}
			}
		}
	}
	
	@EventHandler
	public void UnrankedQueueItemInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(PlayerMode.isInLobbyMode(p)) {
	    if (p.getItemInHand().getType() == Material.IRON_SWORD) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
			if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§2§lUnranked-Queue")) {
				Utils_Shop.openUnranked(p);
					}
				}
			}
		}
	}

	@EventHandler
	public void RankedQueueItemInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (PlayerMode.isInLobbyMode(p)) {
			if (p.getItemInHand().getType() == Material.DIAMOND_SWORD) {
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (e.getItem().getItemMeta().getDisplayName() != null) {
						if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§9§lRanked-Queue")) {
							p.sendMessage("§8[§bEaro§8] §cDu benötigst mindestens 10 Wins.");
							Utils_Shop.openRanked(p);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void SpectateItemInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (PlayerMode.isInLobbyMode(p)) {
			if (p.getItemInHand().getType() == Material.GHAST_TEAR) {
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§b§lSpectate")) {
						p.sendMessage("§8[§bEaro§8] §cBenutze /spec <PLAYERNAME>");
						Utils_Shop.openSpec(p);

					}
				}
			}
		}
	}

	@EventHandler
	public void EditKitsItemInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (PlayerMode.isInLobbyMode(p)) {
			if (p.getItemInHand().getType() == Material.CHEST) {
				if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lEdit-Kit")) {
						Utils_Shop.openEditKit(p);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void LeaveSpectatorMode(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (p.getItemInHand() != null) {
			 if(p.getItemInHand().getType().equals(Material.SLIME_BALL)) {
				if (PlayerMode.isInSpectatorMode(p)) {
					if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
						if (p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lClick to leave")) {
							PlayerMode.setPlayerMode(p, PlayerMode.LOBBY_MODE);
							for (Player players : Bukkit.getOnlinePlayers()) {
								players.showPlayer(p);
							}
							Location loc = new Location(Bukkit.getWorld("practice"), 33.5, 29, 22.5);
							LobbyItems.run(p);
							p.teleport(loc);
							p.setFlying(false);
							p.setAllowFlight(false);
							p.sendMessage(Main.PREFIX + "Du hast den Spectator-Modus verlassen.");
							p.setGameMode(GameMode.SURVIVAL);

						}
					}
				}
			}
		}

	}


	
	@EventHandler
	public void FeastBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
			if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lFeast")) {
				if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					Feast.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void NoDebuffBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lNoDebuff")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					NoDebuff.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void Gapple(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lGapple")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					Gapple.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void EarlyHgBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lEarlyHG")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					EarlyHG.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void DebuffBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lDebuff")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					Debuff.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void BuildUhcBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lBuildUHC")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					BuildUHC.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void SurvivalGamesBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lSG")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					SG.run2(p);
				}
			}
		}
	}
	
	@EventHandler
	public void ArcherBook(PlayerInteractEvent e) {
		Player p = e.getPlayer();
	    if (p.getItemInHand().getType() == Material.ENCHANTED_BOOK) {
	    	if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lArcher")) {
	    		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {			
					Archer.run2(p);
				}
			}
		}
	}
}
