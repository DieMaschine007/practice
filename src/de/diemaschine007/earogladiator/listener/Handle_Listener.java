package de.diemaschine007.earogladiator.listener;

import de.diemaschine007.earogladiator.arenasystem.Arena;
import de.diemaschine007.earogladiator.arenasystem.ArenaManager;
import de.diemaschine007.earogladiator.arenasystem.Arenas;
import de.diemaschine007.earogladiator.arenasystem.Kit;
import de.diemaschine007.earogladiator.main.PlayerMode;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.potion.PotionEffect;

import de.diemaschine007.earogladiator.main.Main;
import de.diemaschine007.earogladiator.methods.SetScoreboard_METHODS;
import de.diemaschine007.earogladiator.utils.LobbyItems;

public class Handle_Listener implements Listener {

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if(PlayerMode.isInLobbyMode(p)) { //PlayerMode.playermode.get(p) == (PlayerMode.LOBBY_MODE())
			if (p.getGameMode() != GameMode.CREATIVE) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPlayerRegenerateHealth(EntityRegainHealthEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			Kit kit = ArenaManager.getArenaFromPlayer(player).getKit();
			if (kit != null) {
				if (kit == Kit.BUILD_UHC) {
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if(PlayerMode.isInLobbyMode(p)) {
			if (p.getGameMode() != GameMode.CREATIVE) {
				e.setCancelled(true);
			}
		} else if(e.getBlock().getType() == Material.GLASS) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onItemClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if(PlayerMode.isInLobbyMode(p)) {
			if (p.getGameMode() != GameMode.CREATIVE) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onItemMove(InventoryMoveItemEvent e) {
		Player p = (Player) e.getItem();
		if(PlayerMode.isInLobbyMode(p)) {
			if (p.getGameMode() != GameMode.CREATIVE) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (PlayerMode.isInLobbyMode(p)) {
			e.setCancelled(true);
		}
	}

    @EventHandler
    public void onFood(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(PlayerMode.isInLobbyMode(p)) {
    		e.setCancelled(true);
		}
    }

	@EventHandler
	public void onPlayerDeathInGame(PlayerDeathEvent event) {
		Player player = event.getEntity().getPlayer();

		Player killer = event.getEntity().getKiller();
		event.setDeathMessage(null);
		event.getDrops().clear();

		if (PlayerMode.isInFightMode(player)) {
			Arena arena = ArenaManager.getArenaFromPlayer(player);
			if (killer == null) {

				Player otherPlayer = arena.otherPlayer(player);
				arena.endGame(otherPlayer);
			} else {
				if (arena != null) {
					ArenaManager.playerToArenaMap.get(player).endGame(killer);
				} else player.sendMessage("DEBUG: Du bist in keiner Arena.");
			}
		}
	}

	@EventHandler
	public void onBlockDamage(EntityDamageByBlockEvent e) {
		if(e.getEntity() instanceof Player ) {
			Player p = (Player) e.getEntity();
			if (PlayerMode.isInLobbyMode(p)) {
				e.setCancelled(true);
			}
		}
	}
    
	@EventHandler
	public void onPlayerDMG(EntityDamageEvent e) {
		Player p = (Player) e.getEntity();
		if(e.getEntity() instanceof Player && PlayerMode.isInLobbyMode(p)) {
			e.setCancelled(true);
		}		   			
	}
	
	@EventHandler
	public void fuckingRain(WeatherChangeEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void fuckingAchievements(PlayerAchievementAwardedEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		Location loc = new Location(Bukkit.getWorld("practice"), 33.5, 28.5, 22.5);
		Player p = e.getPlayer();
		PlayerMode.setPlayerMode(p, PlayerMode.LOBBY_MODE);
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.updateInventory();
		p.setGameMode(GameMode.SURVIVAL);
		p.setMaxHealth(20D);
		LobbyItems.run(p);
		p.teleport(loc);
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.showPlayer(p);
		}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		e.setQuitMessage(null);
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.showPlayer(p);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();

		PlayerMode.setPlayerMode(player, PlayerMode.LOBBY_MODE);

		for (Arena arenas : Arenas.getStandardArenas()) {
			player.sendMessage(arenas.getArenaType().toString());
		}

		SetScoreboard_METHODS.setScoreboard(player);
		SetScoreboard_METHODS.setTab(player);
		Location loc = new Location(Bukkit.getWorld("practice"), 33.5, 28.5, 22.5);
		player.teleport(loc);
		player.setGameMode(GameMode.SURVIVAL);
		player.playSound(player.getLocation(), Sound.NOTE_BASS_DRUM, 10, 9);
		event.setJoinMessage(null);

		FileConfiguration cfg = Main.getPlugin().getConfig();

		if (cfg.get("elo." + player.getUniqueId()) == null) {
			cfg.set("elo." + player.getUniqueId(), 1000);
			Main.getPlugin().saveConfig();
			ArenaManager.playerEloMap.put(player, 1000);
			player.sendMessage("DEBUG: Elo gesetzt");
		} else {
			ArenaManager.playerEloMap.put(player, cfg.getInt("elo." + player.getUniqueId()));
		}

		player.sendMessage("§7§m----------------------------------");
		player.sendMessage("§7                                   ");
		player.sendMessage(Main.PREFIX + "Du bist jetzt bei §2Practice§7.");
		player.sendMessage("§7                                   ");
		player.sendMessage("§7§m----------------------------------");



		player.getInventory().clear();
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);
		player.setLevel(0);
		player.setMaxHealth(20D);
		player.setHealth(20D);
		player.setFoodLevel(20);
		player.setExp(0);
		LobbyItems.run(player);
		for (PotionEffect effect : player.getActivePotionEffects())
			player.removePotionEffect(effect.getType());
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {

			@Override
			public void run() {
				SetScoreboard_METHODS.updateScoreboard(player);
				SetScoreboard_METHODS.updateTab(player);
			}
		}, 0L, 20L);
	}
}
