/*
package de.diemaschine007.earogladiator.listener;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

import de.diemaschine007.earogladiator.main.Main;
import de.diemaschine007.earogladiator.utils.ArenaSystem;
import de.diemaschine007.earogladiator.utils.LeaveQueueItem;
import de.diemaschine007.earogladiator.utils.LobbyItems;

public class FightDeathListener implements Listener {
	
	public static ArrayList<Player> arena1 = new ArrayList<Player>();
	public static ArrayList<Player> arena2 = new ArrayList<Player>();
	public static ArrayList<Player> arena3 = new ArrayList<Player>();
	public static ArrayList<Player> arena4 = new ArrayList<Player>();
	public static ArrayList<Player> arena5 = new ArrayList<Player>();
	public static ArrayList<Player> arena6 = new ArrayList<Player>();
	public static ArrayList<Player> arena7 = new ArrayList<Player>();
	public static ArrayList<Player> arena8 = new ArrayList<Player>();
	public static ArrayList<Player> arena9 = new ArrayList<Player>();
	public static ArrayList<Player> arena10 = new ArrayList<Player>();
	public static ArrayList<Player> arena11 = new ArrayList<Player>();
	public static ArrayList<Player> arena12 = new ArrayList<Player>();
	  
	  Location loc = new Location(Bukkit.getWorld("practice"), 33.5, 28.5, 22.5);
	  
	  int countdown = 10;

		@EventHandler
		public void onDeath(PlayerDeathEvent e) {
			Player p = e.getEntity().getPlayer();
			Player k = e.getEntity().getPlayer().getKiller();
			loc.setPitch(2f);
			loc.setYaw(180f);
			if(p.getKiller() != null) {
				k.sendMessage(Main.PREFIX + "§7Du hast §4" + p.getName() + " §7get§tet!");
				p.sendMessage(Main.PREFIX + "§7Du wurdest von §e" + k.getName() + " §7get§tet!");
				p.playSound(p.getLocation(), Sound.IRONGOLEM_DEATH, 3, 1);
				k.playSound(p.getLocation(), Sound.LEVEL_UP, 3, 1);
				e.setDeathMessage(null);
				ArenaSystem.onArrayClear(p);
			if(arena1.contains(p)) {
	            arena1.remove(k);
	            arena1.remove(p);
			}
			if(arena2.contains(p)) {
	            arena2.remove(k);
	            arena2.remove(p);
				Location locx1 = new Location(Bukkit.getWorld("practice"), 950, 100, 970);
				Location locx2 = new Location(Bukkit.getWorld("practice"), 1050, 149, 1030);
            	int x1 = locx1.getBlockX();
                int y1 = locx1.getBlockY();
                int z1 = locx1.getBlockZ();
                int x2 = locx2.getBlockX();
                int y2 = locx2.getBlockY();
                int z2 = locx2.getBlockZ();

                int minX = Integer.min(x1, x2);
                int minY = Integer.min(y1, y2);
                int minZ = Integer.min(z1, z2);

                int maxX = Integer.max(x1, x2);
                int maxY = Integer.max(y1, y2);
                int maxZ = Integer.max(z1, z2);

                    for (int x = minX; x < maxX + 1; x++) {
                        for (int y = minY; y < maxY + 1; y++) {
                            for (int z = minZ; z < maxZ + 1; z++) {
                                p.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
			}
			if(arena3.contains(p)) {
	            arena3.remove(k);
	            arena3.remove(p);
			}
			if(arena4.contains(p)) {
	            arena4.remove(k);
	            arena4.remove(p);
			}
			if(arena5.contains(p)) {
	            arena5.remove(k);
	            arena5.remove(p);
			}
			if(arena6.contains(p)) {
	            arena6.remove(k);
	            arena6.remove(p);
				Location locx3 = new Location(Bukkit.getWorld("practice"), 2950, 100, 2970);
				Location locx4 = new Location(Bukkit.getWorld("practice"), 3050, 149, 3030);
            	int x1 = locx3.getBlockX();
                int y1 = locx3.getBlockY();
                int z1 = locx3.getBlockZ();
                int x2 = locx4.getBlockX();
                int y2 = locx4.getBlockY();
                int z2 = locx4.getBlockZ();

                int minX = Integer.min(x1, x2);
                int minY = Integer.min(y1, y2);
                int minZ = Integer.min(z1, z2);

                int maxX = Integer.max(x1, x2);
                int maxY = Integer.max(y1, y2);
                int maxZ = Integer.max(z1, z2);

                    for (int x = minX; x < maxX + 1; x++) {
                        for (int y = minY; y < maxY + 1; y++) {
                            for (int z = minZ; z < maxZ + 1; z++) {
                                p.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
			}
			if(arena7.contains(p)) {
	            arena7.remove(k);
	            arena7.remove(p);
			}
			if(arena8.contains(p)) {
	            arena8.remove(k);
	            arena8.remove(p);
			}
			if(arena9.contains(p)) {
	            arena9.remove(k);
	            arena9.remove(p);
			}
			if(arena10.contains(p)) {
	            arena10.remove(k);
	            arena10.remove(p);
			}
			if(arena11.contains(p)) {
	            arena11.remove(k);
	            arena11.remove(p);
			}
			if(arena12.contains(p)) {
	            arena12.remove(k);
	            arena12.remove(p);
			}
	            k.teleport(loc);
	            p.teleport(loc);
	            if(!Main.into100fight.contains(p.getName()) || !Main.lobby.contains(p.getName())) {
				p.playSound(p.getLocation(), Sound.IRONGOLEM_DEATH, 3, 1);
				k.playSound(p.getLocation(), Sound.LEVEL_UP, 3, 1);
				e.setDeathMessage(null);
	            }
	            k.getInventory().clear();
	            k.getInventory().setArmorContents(null);
	            p.getInventory().clear();
	            p.getInventory().setArmorContents(null);
	            ArenaSystem.onArrayClear(p);
	            ArenaSystem.onArrayClear(k);
	            LobbyItems.run(k);
				for (PotionEffect effect : k.getActivePotionEffects())
					k.removePotionEffect(effect.getType());
				k.setFoodLevel(20);
	            LobbyItems.run(p);
	            k.setFireTicks(0);
	            k.setHealth(20D);
	            Main.lobby.add(k.getName());
	            e.getDrops().clear();
			} else {
				e.setDeathMessage(null);
				ArenaSystem.onArrayClear(p);
			if(arena1.contains(p)) {
				arena1.remove(p);
				for(Player arena1player : arena1) {
					arena1player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena1player.teleport(loc);
					arena1player.getInventory().clear();
					arena1player.getInventory().setArmorContents(null);
					arena1player.setFireTicks(0);
					arena1player.setHealth(20D);
					for (PotionEffect effect : arena1player.getActivePotionEffects())
						arena1player.removePotionEffect(effect.getType());
					arena1player.setFoodLevel(20);
					LobbyItems.run(arena1player);
		            ArenaSystem.onArrayClear(arena1player);
		            if(!Main.lobby.contains(arena1player.getName())) {
			            Main.lobby.add(arena1player.getName());
			            }
					arena1.clear();
		            e.getDrops().clear();
				}
			}
			if(arena2.contains(p)) {
				Location locx1 = new Location(Bukkit.getWorld("practice"), 950, 100, 970);
				Location locx2 = new Location(Bukkit.getWorld("practice"), 1050, 149, 1030);
				arena2.remove(p);
            	int x1 = locx1.getBlockX();
                int y1 = locx1.getBlockY();
                int z1 = locx1.getBlockZ();
                int x2 = locx2.getBlockX();
                int y2 = locx2.getBlockY();
                int z2 = locx2.getBlockZ();

                int minX = Integer.min(x1, x2);
                int minY = Integer.min(y1, y2);
                int minZ = Integer.min(z1, z2);

                int maxX = Integer.max(x1, x2);
                int maxY = Integer.max(y1, y2);
                int maxZ = Integer.max(z1, z2);

                    for (int x = minX; x < maxX + 1; x++) {
                        for (int y = minY; y < maxY + 1; y++) {
                            for (int z = minZ; z < maxZ + 1; z++) {
                                p.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
				for(Player arena2player : arena2) {
					arena2player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena2player.teleport(loc);
					arena2player.getInventory().clear();
					arena2player.getInventory().setArmorContents(null);
					arena2player.setFireTicks(0);
					arena2player.setHealth(20D);
					for (PotionEffect effect : arena2player.getActivePotionEffects())
						arena2player.removePotionEffect(effect.getType());
					arena2player.setFoodLevel(20);
					LobbyItems.run(arena2player);
		            ArenaSystem.onArrayClear(arena2player);
		            if(!Main.lobby.contains(arena2player.getName())) {
			            Main.lobby.add(arena2player.getName());
			            }
					arena2.clear();
		            e.getDrops().clear();
				}
			}
			if(arena3.contains(p)) {
				arena3.remove(p);
				for(Player arena3player : arena3) {
					arena3player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena3player.teleport(loc);
					arena3player.getInventory().clear();
					arena3player.getInventory().setArmorContents(null);
					arena3player.setFireTicks(0);
					arena3player.setHealth(20D);
					for (PotionEffect effect : arena3player.getActivePotionEffects())
						arena3player.removePotionEffect(effect.getType());
					arena3player.setFoodLevel(20);
					LobbyItems.run(arena3player);
		            ArenaSystem.onArrayClear(arena3player);
		            if(!Main.lobby.contains(arena3player.getName())) {
			            Main.lobby.add(arena3player.getName());
			            }
					arena3.clear();
		            e.getDrops().clear();
				}
			}
			if(arena4.contains(p)) {
				arena4.remove(p);
				for(Player arena4player : arena4) {
					arena4player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena4player.teleport(loc);
					arena4player.getInventory().clear();
					arena4player.getInventory().setArmorContents(null);
					arena4player.setFireTicks(0);
					arena4player.setHealth(20D);
					for (PotionEffect effect : arena4player.getActivePotionEffects())
						arena4player.removePotionEffect(effect.getType());
					arena4player.setFoodLevel(20);
					LobbyItems.run(arena4player);
		            ArenaSystem.onArrayClear(arena4player);
		            if(!Main.lobby.contains(arena4player.getName())) {
			            Main.lobby.add(arena4player.getName());
			            }
					arena4.clear();
		            e.getDrops().clear();
				}
			}
			if(arena5.contains(p)) {
				arena5.remove(p);
				for(Player arena5player : arena5) {
					arena5player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena5player.teleport(loc);
					arena5player.getInventory().clear();
					arena5player.getInventory().setArmorContents(null);
					arena5player.setFireTicks(0);
					arena5player.setHealth(20D);
					for (PotionEffect effect : arena5player.getActivePotionEffects())
						arena5player.removePotionEffect(effect.getType());
					arena5player.setFoodLevel(20);
					LobbyItems.run(arena5player);
		            ArenaSystem.onArrayClear(arena5player);
		            if(!Main.lobby.contains(arena5player.getName())) {
			            Main.lobby.add(arena5player.getName());
			            }
					arena5.clear();
		            e.getDrops().clear();
				}
			}
			if(arena6.contains(p)) {
				Location locx3 = new Location(Bukkit.getWorld("practice"), 2950, 100, 2970);
				Location locx4 = new Location(Bukkit.getWorld("practice"), 3050, 149, 3030);
				arena6.remove(p);
            	int x1 = locx3.getBlockX();
                int y1 = locx3.getBlockY();
                int z1 = locx3.getBlockZ();
                int x2 = locx4.getBlockX();
                int y2 = locx4.getBlockY();
                int z2 = locx4.getBlockZ();

                int minX = Integer.min(x1, x2);
                int minY = Integer.min(y1, y2);
                int minZ = Integer.min(z1, z2);

                int maxX = Integer.max(x1, x2);
                int maxY = Integer.max(y1, y2);
                int maxZ = Integer.max(z1, z2);

                    for (int x = minX; x < maxX + 1; x++) {
                        for (int y = minY; y < maxY + 1; y++) {
                            for (int z = minZ; z < maxZ + 1; z++) {
                                p.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
				for(Player arena6player : arena6) {
					arena6player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena6player.teleport(loc);
					arena6player.getInventory().clear();
					arena6player.getInventory().setArmorContents(null);
					arena6player.setFireTicks(0);
					arena6player.setHealth(20D);
					for (PotionEffect effect : arena6player.getActivePotionEffects())
						arena6player.removePotionEffect(effect.getType());
					arena6player.setFoodLevel(20);
					LobbyItems.run(arena6player);
		            ArenaSystem.onArrayClear(arena6player);
		            if(!Main.lobby.contains(arena6player.getName())) {
			            Main.lobby.add(arena6player.getName());
			            }
					arena6.clear();
		            e.getDrops().clear();
				}
			}
			if(arena7.contains(p)) {
				arena7.remove(p);
				for(Player arena7player : arena7) {
					arena7player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena7player.teleport(loc);
					arena7player.getInventory().clear();
					arena7player.getInventory().setArmorContents(null);
					arena7player.setFireTicks(0);
					arena7player.setHealth(20D);
					for (PotionEffect effect : arena7player.getActivePotionEffects())
						arena7player.removePotionEffect(effect.getType());
					arena7player.setFoodLevel(20);
					LobbyItems.run(arena7player);
		            ArenaSystem.onArrayClear(arena7player);
		            if(!Main.lobby.contains(arena7player.getName())) {
			            Main.lobby.add(arena7player.getName());
			            }
					arena7.clear();
		            e.getDrops().clear();
				}
			}
			if(arena8.contains(p)) {
				arena8.remove(p);
				for(Player arena8player : arena8) {
					arena8player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena8player.teleport(loc);
					arena8player.getInventory().clear();
					arena8player.getInventory().setArmorContents(null);
					arena8player.setFireTicks(0);
					arena8player.setHealth(20D);
					for (PotionEffect effect : arena8player.getActivePotionEffects())
						arena8player.removePotionEffect(effect.getType());
					arena8player.setFoodLevel(20);
					LobbyItems.run(arena8player);
		            ArenaSystem.onArrayClear(arena8player);
		            if(!Main.lobby.contains(arena8player.getName())) {
			            Main.lobby.add(arena8player.getName());
			            }
					arena8.clear();
		            e.getDrops().clear();
				}
			}
			if(arena9.contains(p)) {
				arena9.remove(p);
				for(Player arena9player : arena9) {
					arena9player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena9player.teleport(loc);
					arena9player.getInventory().clear();
					arena9player.getInventory().setArmorContents(null);
					arena9player.setFireTicks(0);
					arena9player.setHealth(20D);
					for (PotionEffect effect : arena9player.getActivePotionEffects())
						arena9player.removePotionEffect(effect.getType());
					arena9player.setFoodLevel(20);
					LobbyItems.run(arena9player);
		            ArenaSystem.onArrayClear(arena9player);
		            if(!Main.lobby.contains(arena9player.getName())) {
			            Main.lobby.add(arena9player.getName());
			            }		  
					arena9.clear();
		            e.getDrops().clear();
				}
			}
			if(arena10.contains(p)) {
				arena10.remove(p);
				for(Player arena10player : arena10) {
					arena10player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena10player.teleport(loc);
					arena10player.getInventory().clear();
					arena10player.getInventory().setArmorContents(null);
					arena10player.setFireTicks(0);
					arena10player.setHealth(20D);
					for (PotionEffect effect : arena10player.getActivePotionEffects())
						arena10player.removePotionEffect(effect.getType());
					arena10player.setFoodLevel(20);
					LobbyItems.run(arena10player);
		            ArenaSystem.onArrayClear(arena10player);
		            if(!Main.lobby.contains(arena10player.getName())) {
			            Main.lobby.add(arena10player.getName());
			            }
					arena10.clear();
		            e.getDrops().clear();
				}
			}
			if(arena11.contains(p)) {
				arena11.remove(p);
				for(Player arena11player : arena11) {
					arena11player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena11player.teleport(loc);
					arena11player.getInventory().clear();
					arena11player.getInventory().setArmorContents(null);
					arena11player.setFireTicks(0);
					arena11player.setHealth(20D);
					for (PotionEffect effect : arena11player.getActivePotionEffects())
						arena11player.removePotionEffect(effect.getType());
					arena11player.setFoodLevel(20);
					LobbyItems.run(arena11player);
		            ArenaSystem.onArrayClear(arena11player);
		            if(!Main.lobby.contains(arena11player.getName())) {
			            Main.lobby.add(arena11player.getName());
			            }
					arena11.clear();
		            e.getDrops().clear();
				}
			}
			if(arena12.contains(p)) {
				arena12.remove(p);
				for(Player arena12player : arena12) {
					arena12player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena12player.teleport(loc);
					arena12player.getInventory().clear();
					arena12player.getInventory().setArmorContents(null);
					arena12player.setFireTicks(0);
					arena12player.setHealth(20D);
					for (PotionEffect effect : arena12player.getActivePotionEffects())
						arena12player.removePotionEffect(effect.getType());
					arena12player.setFoodLevel(20);
					LobbyItems.run(arena12player);
		            ArenaSystem.onArrayClear(arena12player);
		            if(!Main.lobby.contains(arena12player.getName())) {
			            Main.lobby.add(arena12player.getName());
			            }
					arena12.clear();
		            e.getDrops().clear();
				}
			}
            p.teleport(loc);
            if(!Main.into100fight.contains(p.getName())) {
            	p.playSound(p.getLocation(), Sound.IRONGOLEM_DEATH, 3, 1);
            	e.setDeathMessage(null);
            }
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            e.getDrops().clear();
			p.sendMessage(Main.PREFIX + "§7Du bist gestorben!");
			p.playSound(p.getLocation(), Sound.IRONGOLEM_DEATH, 3, 1);
			}
		}
		
		@EventHandler
		public void onClick1(InventoryClickEvent e) {
			Location loc1 = new Location(Bukkit.getWorld("practice"), 548.5, 101, 500.5);
			Location loc2 = new Location(Bukkit.getWorld("practice"), 452.5, 101, 500.5);
			loc1.setPitch(2f);
			loc1.setYaw(90f);
			loc2.setPitch(2f);
			loc2.setYaw(270f);
			Location loc3 = new Location(Bukkit.getWorld("practice"), 1048.5, 101, 1000.5);
			Location loc4 = new Location(Bukkit.getWorld("practice"), 952.5, 101, 1000.5);
			loc3.setPitch(2f);
			loc3.setYaw(90f);
			loc4.setPitch(2f);
			loc4.setYaw(270f);
			Location loc5 = new Location(Bukkit.getWorld("practice"), 1548.5, 101, 1500.5);
			Location loc6 = new Location(Bukkit.getWorld("practice"), 1452.5, 101, 1500.5);
			loc5.setPitch(2f);
			loc5.setYaw(90f);
			loc6.setPitch(2f);
			loc6.setYaw(270f);
			Location loc7 = new Location(Bukkit.getWorld("practice"), 2048.5, 101, 2000.5);
			Location loc8 = new Location(Bukkit.getWorld("practice"), 1952.5, 101, 2000.5);
			loc7.setPitch(2f);
			loc7.setYaw(90f);
			loc8.setPitch(2f);
			loc8.setYaw(270f);
			Location loc9 = new Location(Bukkit.getWorld("practice"), 2548.5, 101, 2500.5);
			Location loc10 = new Location(Bukkit.getWorld("practice"), 2452.5, 101, 2500.5);
			loc9.setPitch(2f);
			loc9.setYaw(90f);
			loc10.setPitch(2f);
			loc10.setYaw(270f);
				  
			Location loc11 = new Location(Bukkit.getWorld("practice"), 3048.5, 101, 3000.5);
			Location loc12 = new Location(Bukkit.getWorld("practice"), 2952.5, 101, 3000.5);
			loc11.setPitch(2f);
			loc11.setYaw(90f);
			loc12.setPitch(2f);
			loc12.setYaw(270f);
			Location loc13 = new Location(Bukkit.getWorld("practice"), 3548.5, 101, 3500.5);
			Location loc14 = new Location(Bukkit.getWorld("practice"), 3452.5, 101, 3500.5);
			loc13.setPitch(2f);
			loc13.setYaw(90f);
			loc14.setPitch(2f);
			loc14.setYaw(270f);
			Location loc15 = new Location(Bukkit.getWorld("practice"), 62.5, 20, 29.5);
			Location loc16 = new Location(Bukkit.getWorld("practice"), 70.5, 20, 37.5);
			loc15.setPitch(2f);
			loc15.setYaw(90f);
			loc16.setPitch(2f);
			loc16.setYaw(270f);
			Location loc17 = new Location(Bukkit.getWorld("practice"), 4496.5, 3.5, 4500.5);
			Location loc18 = new Location(Bukkit.getWorld("practice"), 4504.5, 3.5, 4500.5);
			loc17.setPitch(2f);
			loc17.setYaw(270f);
			loc18.setPitch(2f);
			loc18.setYaw(90f);
			Location loc19 = new Location(Bukkit.getWorld("practice"), 5048.5, 101, 5000.5);
			Location loc20 = new Location(Bukkit.getWorld("practice"), 4952.5, 101, 5000.5);
			loc19.setPitch(2f);
			loc19.setYaw(90f);
			loc20.setPitch(2f);
			loc20.setYaw(270f);
			Location loc21 = new Location(Bukkit.getWorld("practice"), 5548.5, 101, 5500.5);
			Location loc22 = new Location(Bukkit.getWorld("practice"), 5452.5, 101, 5500.5);
			loc21.setPitch(2f);
			loc21.setYaw(90f);
			loc22.setPitch(2f);
			loc22.setYaw(270f);
			Location loc23 = new Location(Bukkit.getWorld("practice"), 6048.5, 101, 6000.5);
			Location loc24 = new Location(Bukkit.getWorld("practice"), 5952.5, 101, 6000.5);
			loc23.setPitch(2f);
			loc23.setYaw(90f);
			loc24.setPitch(2f);
			loc24.setYaw(270f);
			
			
			Player p = (Player) e.getWhoClicked();
			if(e.getClickedInventory() != null) {
			if(e.getInventory().getName().equalsIgnoreCase("§2Unranked-Kit")) {
				if(e.getCurrentItem().getType().equals(Material.REDSTONE)) {
					e.getView().close();
					e.setCancelled(true);
				} else if(e.getCurrentItem().getType().equals(Material.MUSHROOM_SOUP)) {
					if(!Main.infeastqueue.isEmpty()) {
						if(arena1.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onFeastTP(p, arena1, loc1, loc2);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.infeastqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aFeast-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.LAVA_BUCKET)) {
					if(!Main.inbuilduhcqueue.isEmpty()) {
						if(arena2.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onBuildUHCTP(p, arena2, loc3, loc4);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.inbuilduhcqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aBuildUHC-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("§aNoDebuff")) {
						if(!Main.innodebuffqueue.isEmpty()) {
							if(arena3.isEmpty()) {
								e.setCancelled(true);
								ArenaSystem.onNoDebuffTP(p, arena3, loc5, loc6);
							} else {
								p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
							}
						} else {
						Main.innodebuffqueue.add(p.getName());
						e.setCancelled(true);
						p.updateInventory();
						p.closeInventory();
			            p.setFireTicks(0);
			            p.setHealth(20D);
			            p.getInventory().clear();
			            p.getInventory().setArmorContents(null);
			            LeaveQueueItem.run(p);
			            p.sendMessage(Main.PREFIX + "Du bist der §aNoDebuff-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.GOLDEN_APPLE)) {
					if(!Main.ingapplequeue.isEmpty()) {
						if(arena4.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onGappleTP(p, arena4, loc7, loc8);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.ingapplequeue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aGapple-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.STONE_SWORD)) {
					if(!Main.inearlyhgqueue.isEmpty()) {
						if(arena5.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onEarlyHGTP(p, arena5, loc9, loc10);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.inearlyhgqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aEarlyHG-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.FISHING_ROD)) {
					if(!Main.insgqueue.isEmpty()) {
						if(arena6.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onSgTp(p, arena6, loc11, loc12);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.insgqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aSG-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("§aDebuff")) {
						if(!Main.indebuffqueue.isEmpty()) {
							if(arena7.isEmpty()) {
								e.setCancelled(true);
								ArenaSystem.onDebuffTP(p, arena7, loc13, loc14);
							} else {
								p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
							}
						} else {
						Main.indebuffqueue.add(p.getName());
						e.setCancelled(true);
						p.updateInventory();
						p.closeInventory();
			            p.setFireTicks(0);
			            p.setHealth(20D);
			            p.getInventory().clear();
			            p.getInventory().setArmorContents(null);
			            LeaveQueueItem.run(p);
			            p.sendMessage(Main.PREFIX + "Du bist der §aDebuff-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.BEACON)) {
					Main.insoupwarsqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aSoupWars1vs1-Kit §7beigetreten.");
				} else if(e.getCurrentItem().getType().equals(Material.LEASH)) {
					if(!Main.insumoqueue.isEmpty()) {
						if(arena9.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onSumoTP(p, arena9, loc17, loc18);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.insumoqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aSumo-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.BEDROCK)) {
					if(!Main.into100queue.isEmpty()) {
						if(arena10.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onto100TP(p, arena10, loc19, loc20);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.into100queue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aTo100-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("§aTo100-Speed")) {
					if(!Main.into100speedqueue.isEmpty()) {
						if(arena11.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onto100SpeedTP(p, arena11, loc21, loc22);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.into100speedqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aTo100Speed-Kit §7beigetreten.");
					}
				} else if(e.getCurrentItem().getType().equals(Material.BOW)) {
					if(!Main.inarcherqueue.isEmpty()) {
						if(arena12.isEmpty()) {
							e.setCancelled(true);
							ArenaSystem.onArcherTP(p, arena12, loc23, loc24);
						} else {
							p.sendMessage(Main.PREFIX + "§cKeine Arena ist frei, warte einen Moment!");
						}
					} else {
					Main.inarcherqueue.add(p.getName());
					e.setCancelled(true);
					p.updateInventory();
					p.closeInventory();
		            p.setFireTicks(0);
		            p.setHealth(20D);
		            p.getInventory().clear();
		            p.getInventory().setArmorContents(null);
		            LeaveQueueItem.run(p);
		            p.sendMessage(Main.PREFIX + "Du bist der §aArcher-Kit §7beigetreten.");
					}
				} else {
					e.setCancelled(true);
					}
				}
			}
		}
		
		@EventHandler
		public void onLeave(PlayerQuitEvent e) { 
			Player p = e.getPlayer();
            ArenaSystem.onArrayClear(p);
			for (PotionEffect effect : p.getActivePotionEffects())
				p.removePotionEffect(effect.getType());
			p.setFoodLevel(20);
			if(arena1.contains(p)) {
				arena1.remove(p);
				for(Player arena1player : arena1) {
					arena1player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena1player.teleport(loc);
		            ArenaSystem.onArrayClear(arena1player);
					arena1player.getInventory().clear();
					arena1player.getInventory().setArmorContents(null);
					arena1player.setFireTicks(0);
					arena1player.setHealth(20D);
					for (PotionEffect effect : arena1player.getActivePotionEffects())
						arena1player.removePotionEffect(effect.getType());
					arena1player.setFoodLevel(20);
					LobbyItems.run(arena1player);
		            Main.lobby.add(arena1player.getName());
					arena1player.setLevel(0);
					arena1.clear();
				}
			}
			if(arena2.contains(p)) {
				arena2.remove(p);
				Location locx1 = new Location(Bukkit.getWorld("practice"), 950, 100, 970);
				Location locx2 = new Location(Bukkit.getWorld("practice"), 1050, 149, 1030);
            	int x1 = locx1.getBlockX();
                int y1 = locx1.getBlockY();
                int z1 = locx1.getBlockZ();
                int x2 = locx2.getBlockX();
                int y2 = locx2.getBlockY();
                int z2 = locx2.getBlockZ();

                int minX = Integer.min(x1, x2);
                int minY = Integer.min(y1, y2);
                int minZ = Integer.min(z1, z2);

                int maxX = Integer.max(x1, x2);
                int maxY = Integer.max(y1, y2);
                int maxZ = Integer.max(z1, z2);

                    for (int x = minX; x < maxX + 1; x++) {
                        for (int y = minY; y < maxY + 1; y++) {
                            for (int z = minZ; z < maxZ + 1; z++) {
                                p.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
				for(Player arena2player : arena2) {
					arena2player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena2player.teleport(loc);
					ArenaSystem.onArrayClear(arena2player);
					arena2player.getInventory().clear();
					arena2player.getInventory().setArmorContents(null);
					arena2player.setFireTicks(0);
					arena2player.setHealth(20D);
					for (PotionEffect effect : arena2player.getActivePotionEffects())
						arena2player.removePotionEffect(effect.getType());
					arena2player.setFoodLevel(20);
					LobbyItems.run(arena2player);
		            Main.lobby.add(arena2player.getName());
					arena2player.setLevel(0);
					arena2.clear();
				}
			}
			if(arena3.contains(p)) {
				arena3.remove(p);
				for(Player arena3player : arena3) {
					arena3player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena3player.teleport(loc);
					ArenaSystem.onArrayClear(arena3player);
					arena3player.getInventory().clear();
					arena3player.getInventory().setArmorContents(null);
					arena3player.setFireTicks(0);
					arena3player.setHealth(20D);
					for (PotionEffect effect : arena3player.getActivePotionEffects())
						arena3player.removePotionEffect(effect.getType());
					arena3player.setFoodLevel(20);
					LobbyItems.run(arena3player);
		            Main.lobby.add(arena3player.getName());
					arena3player.setLevel(0);
					arena3.clear();
				}
			}
			if(arena4.contains(p)) {
				arena4.remove(p);
				for(Player arena4player : arena4) {
					arena4player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena4player.teleport(loc);
					ArenaSystem.onArrayClear(arena4player);
					arena4player.getInventory().clear();
					arena4player.getInventory().setArmorContents(null);
					arena4player.setFireTicks(0);
					arena4player.setHealth(20D);
					for (PotionEffect effect : arena4player.getActivePotionEffects())
						arena4player.removePotionEffect(effect.getType());
					arena4player.setFoodLevel(20);
					LobbyItems.run(arena4player);
		            Main.lobby.add(arena4player.getName());
					arena4player.setLevel(0);
					arena4.clear();
				}
			}
			if(arena5.contains(p)) {
				arena5.remove(p);
				for(Player arena5player : arena5) {
					arena5player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena5player.teleport(loc);
					ArenaSystem.onArrayClear(arena5player);
					arena5player.getInventory().clear();
					arena5player.getInventory().setArmorContents(null);
					arena5player.setFireTicks(0);
					arena5player.setHealth(20D);
					for (PotionEffect effect : arena5player.getActivePotionEffects())
						arena5player.removePotionEffect(effect.getType());
					arena5player.setFoodLevel(20);
					LobbyItems.run(arena5player);
		            Main.lobby.add(arena5player.getName());
					arena5player.setLevel(0);
					arena5.clear();
				}
			}
			if(arena6.contains(p)) {
				arena6.remove(p);
				Location locx3 = new Location(Bukkit.getWorld("practice"), 2950, 100, 2970);
				Location locx4 = new Location(Bukkit.getWorld("practice"), 3050, 149, 3030);
            	int x1 = locx3.getBlockX();
                int y1 = locx3.getBlockY();
                int z1 = locx3.getBlockZ();
                int x2 = locx4.getBlockX();
                int y2 = locx4.getBlockY();
                int z2 = locx4.getBlockZ();

                int minX = Integer.min(x1, x2);
                int minY = Integer.min(y1, y2);
                int minZ = Integer.min(z1, z2);

                int maxX = Integer.max(x1, x2);
                int maxY = Integer.max(y1, y2);
                int maxZ = Integer.max(z1, z2);

                    for (int x = minX; x < maxX + 1; x++) {
                        for (int y = minY; y < maxY + 1; y++) {
                            for (int z = minZ; z < maxZ + 1; z++) {
                                p.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
				for(Player arena6player : arena6) {
					arena6player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena6player.teleport(loc);
					ArenaSystem.onArrayClear(arena6player);
					arena6player.getInventory().clear();
					arena6player.getInventory().setArmorContents(null);
					arena6player.setFireTicks(0);
					arena6player.setHealth(20D);
					for (PotionEffect effect : arena6player.getActivePotionEffects())
						arena6player.removePotionEffect(effect.getType());
					arena6player.setFoodLevel(20);
					LobbyItems.run(arena6player);
		            Main.lobby.add(arena6player.getName());
					arena6player.setLevel(0);
					arena6.clear();
				}
			}
			if(arena7.contains(p)) {
				arena7.remove(p);
				for(Player arena7player : arena7) {
					arena7player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena7player.teleport(loc);
					ArenaSystem.onArrayClear(arena7player);
					arena7player.getInventory().clear();
					arena7player.getInventory().setArmorContents(null);
					arena7player.setFireTicks(0);
					arena7player.setHealth(20D);
					for (PotionEffect effect : arena7player.getActivePotionEffects())
						arena7player.removePotionEffect(effect.getType());
					arena7player.setFoodLevel(20);
					LobbyItems.run(arena7player);
		            Main.lobby.add(arena7player.getName());
					arena7player.setLevel(0);
					arena7.clear();
				}
			}
			if(arena8.contains(p)) {
				arena8.remove(p);
				for(Player arena8player : arena8) {
					arena8player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena8player.teleport(loc);
					ArenaSystem.onArrayClear(arena8player);
					arena8player.getInventory().clear();
					arena8player.getInventory().setArmorContents(null);
					arena8player.setFireTicks(0);
					arena8player.setHealth(20D);
					for (PotionEffect effect : arena8player.getActivePotionEffects())
						arena8player.removePotionEffect(effect.getType());
					arena8player.setFoodLevel(20);
					LobbyItems.run(arena8player);
		            Main.lobby.add(arena8player.getName());
					arena8player.setLevel(0);
					arena8.clear();
				}
			}
			if(arena9.contains(p)) {
				arena9.remove(p);
				for(Player arena9player : arena9) {
					arena9player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena9player.teleport(loc);
					ArenaSystem.onArrayClear(arena9player);
					arena9player.getInventory().clear();
					arena9player.getInventory().setArmorContents(null);
					arena9player.setFireTicks(0);
					arena9player.setHealth(20D);
					for (PotionEffect effect : arena9player.getActivePotionEffects())
						arena9player.removePotionEffect(effect.getType());
					arena9player.setFoodLevel(20);
					LobbyItems.run(arena9player);
		            Main.lobby.add(arena9player.getName());
					arena9player.setLevel(0);
					arena9.clear();
				}
			}
			if(arena10.contains(p)) {
				arena10.remove(p);
				for(Player arena10player : arena10) {
					arena10player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena10player.teleport(loc);
					ArenaSystem.onArrayClear(arena10player);
					arena10player.getInventory().clear();
					arena10player.getInventory().setArmorContents(null);
					arena10player.setFireTicks(0);
					arena10player.setHealth(20D);
					for (PotionEffect effect : arena10player.getActivePotionEffects())
						arena10player.removePotionEffect(effect.getType());
					arena10player.setFoodLevel(20);
					LobbyItems.run(arena10player);
		            Main.lobby.add(arena10player.getName());
					arena10player.setLevel(0);
					arena10.clear();
				}
			}
			if(arena11.contains(p)) {
				arena11.remove(p);
				for(Player arena11player : arena11) {
					arena11player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena11player.teleport(loc);
					ArenaSystem.onArrayClear(arena11player);
					arena11player.getInventory().clear();
					arena11player.getInventory().setArmorContents(null);
					arena11player.setFireTicks(0);
					arena11player.setHealth(20D);
					for (PotionEffect effect : arena11player.getActivePotionEffects())
						arena11player.removePotionEffect(effect.getType());
					arena11player.setFoodLevel(20);
					LobbyItems.run(arena11player);
		            if(!Main.lobby.contains(arena11player.getName())) {
			            Main.lobby.add(arena11player.getName());
			            }
					arena11.clear();
				}
			}
			if(arena12.contains(p)) {
				arena12.remove(p);
				for(Player arena12player : arena12) {
					arena12player.sendMessage(Main.PREFIX + "Du hast den Fight gewonnen!");
					arena12player.teleport(loc);
					ArenaSystem.onArrayClear(arena12player);
					arena12player.getInventory().clear();
					arena12player.getInventory().setArmorContents(null);
					arena12player.setFireTicks(0);
					arena12player.setHealth(20D);
					for (PotionEffect effect : arena12player.getActivePotionEffects())
						arena12player.removePotionEffect(effect.getType());
					arena12player.setFoodLevel(20);
					LobbyItems.run(arena12player);
		            if(!Main.lobby.contains(arena12player.getName())) {
			            Main.lobby.add(arena12player.getName());
			            }
					arena12.clear();
				}
			}
			e.setQuitMessage(null);
			p.getInventory().clear();
			p.getInventory().setHelmet(null);
			p.getInventory().setChestplate(null);
			p.getInventory().setLeggings(null);
			p.getInventory().setBoots(null);
			p.setLevel(0);
			p.setMaxHealth(20D);
			p.setHealth(20D);
			p.setExp(0);
			for (PotionEffect effect : p.getActivePotionEffects())
				p.removePotionEffect(effect.getType());
		}
	}
*/
