/**
package de.diemaschine007.earogladiator.listener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.diemaschine007.earogladiator.main.Main;

public class Listener_Kits implements Listener {
    
    @EventHandler
    public void onFood1(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.infeastfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBreak1(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.infeastfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild1(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.infeastfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop1(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.infeastfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup1(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.infeastfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
    @EventHandler
    public void onFood2(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBreak2(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
    		if(e.getBlock().getType() == Material.COBBLESTONE || e.getBlock().getType() == Material.WOOD || e.getBlock().getType() == Material.BEDROCK || e.getBlock().getType() == Material.FIRE) {
    			e.setCancelled(false);
    		} else {
    			e.setCancelled(true);
    		}
		}
    }
    
    @EventHandler
    public void onBuild2(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
	
	@EventHandler
	public void onIDrop2(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup2(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onPlayerHeal2(EntityRegainHealthEvent e) {
		Player p = (Player) e.getEntity();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
    		if(e.getRegainReason() != null && e.getRegainReason() == RegainReason.EATING || e.getRegainReason() == RegainReason.MAGIC || e.getRegainReason() == RegainReason.MAGIC_REGEN) {
    			e.setCancelled(false);
    		} else {
    			e.setCancelled(true);
    		}
		}
	}
	
	@EventHandler
	public void onConsume2(PlayerItemConsumeEvent e) {
		Player p = e.getPlayer();
    	if(Main.inbuilduhcfight.contains(p.getName())) {
    		if(e.getItem().getType() == Material.GOLDEN_APPLE) {
    			if(e.getItem().getItemMeta().getDisplayName() == "§6Golden Head") {
    				for (PotionEffect effects : p.getActivePotionEffects()) {
    					p.removePotionEffect(effects.getType());
    			         }
    				p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 120 * 20, 0));
    				p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 10 * 20, 1));
    			}
    		}
		}
	}
	
    @EventHandler
    public void onFood3(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.innodebufffight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBreak3(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.innodebufffight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild3(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.innodebufffight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop3(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.innodebufffight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup3(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.innodebufffight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
    @EventHandler
    public void onFood4(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.ingapplefight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBreak4(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.ingapplefight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild4(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.ingapplefight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop4(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.ingapplefight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup4(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.ingapplefight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onConsume4(PlayerItemConsumeEvent e) {
		Player p = e.getPlayer();
    	if(Main.ingapplefight.contains(p.getName())) {
    		if(e.getItem().getType() == Material.POTION) {
    			if(e.getItem().getItemMeta().getDisplayName() == "§dMystical Potion") {
    				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 480 * 20, 0));
    				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 480 * 20, 1));
    			}
    		}
		}
	}
	
    @EventHandler
    public void onFood5(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.inearlyhgfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBreak5(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.inearlyhgfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild5(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.inearlyhgfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop5(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.inearlyhgfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup5(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.inearlyhgfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
    @EventHandler
    public void onFood6(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.insgfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBreak6(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.insgfight.contains(p.getName())) {
    		if(e.getBlock().getType() == Material.WEB || e.getBlock().getType() == Material.FIRE) {
    			e.setCancelled(false);
    		} else {
    			e.setCancelled(true);
    		}
		}
    }
    
    @EventHandler
    public void onBuild6(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.insgfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
	
	@EventHandler
	public void onIDrop6(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.insgfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup6(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.insgfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
    @EventHandler
    public void onFood7(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.indebufffight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBreak7(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.indebufffight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild7(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.indebufffight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop7(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.indebufffight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup7(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.indebufffight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
    @EventHandler
    public void onFood8(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.insoupwarsfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBreak8(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.insoupwarsfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBuild8(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.insoupwarsfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
	
	@EventHandler
	public void onIDrop8(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.insoupwarsfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onIPickup8(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.insoupwarsfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
	
    @EventHandler
    public void onFood9(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.insumofight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBreak9(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.insumofight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild9(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.insumofight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop9(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.insumofight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onIPickup9(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.insumofight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerDMG9(EntityDamageByEntityEvent e) {
    	if(e.getCause() == DamageCause.ENTITY_ATTACK) {
		Player p = (Player) e.getEntity();
		Player k = (Player) e.getDamager();
		if(Main.insumofight.contains(p.getName())) {
			if(e.getDamager() instanceof Player && Main.insumofight.contains(k.getName())) {
				if((k.getItemInHand().getType() == Material.AIR)) {
			
					double origDamage = e.getDamage();
					e.setDamage(origDamage * .0);
					p.setHealth(20.0D);
					}
		    	}
		    }
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {	
	if(e.getTo().getY() <= -3 && !e.getPlayer().isDead()) {
		e.getPlayer().setHealth(0.0D);
		}
	}
	
    @EventHandler
    public void onFood10(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.into100fight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBreak10(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.into100fight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild10(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.into100fight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop10(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.into100fight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onIPickup10(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.into100fight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerDMG10(EntityDamageByEntityEvent e) {
    	if(e.getCause() == DamageCause.ENTITY_ATTACK) {
		Player p = (Player) e.getEntity();
		Player k = (Player) e.getDamager();
		int level = k.getLevel();
		int level2 = p.getLevel();
		if(Main.into100fight.contains(p.getName())) {
			if(e.getDamager() instanceof Player && Main.into100fight.contains(k.getName())) {
					double origDamage = e.getDamage();
					e.setDamage(origDamage * 0);
					if(k.getLevel() <= 98) {
						int level3 = level + 1;
						k.setLevel(level3);
					} else if(k.getLevel() >= 99) {
						p.sendMessage(Main.PREFIX + "§c" + k.getName() + " §7hat vor dir die §a100 §7Level erreicht");
						k.sendMessage(Main.PREFIX + "§e" + p.getName() + " §7hatte erst §a" + level2 + " §7Level.");
						if(Main.into100fight.contains(k.getName())) {
							p.damage(20D);
							k.setLevel(0);
							p.setLevel(0);
						}
					}
		    	}
		    }
		}
	}
	
    @EventHandler
    public void onFood11(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.into100speedfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBreak11(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.into100speedfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild11(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.into100speedfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop11(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.into100speedfight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onIPickup11(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.into100speedfight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerDMG11(EntityDamageByEntityEvent e) {
    	if(e.getCause() == DamageCause.ENTITY_ATTACK) {
		Player p = (Player) e.getEntity();
		Player k = (Player) e.getDamager();
		int level = k.getLevel();
		int level2 = p.getLevel();
		if(Main.into100speedfight.contains(p.getName())) {
			if(e.getDamager() instanceof Player && Main.into100speedfight.contains(k.getName())) {
			
					double origDamage = e.getDamage();
					e.setDamage(origDamage * 0);
					if(k.getLevel() <= 98) {
						int level3 = level + 1;
						k.setLevel(level3);
					} else if(k.getLevel() >= 99) {
						k.setLevel(0);
						p.sendMessage(Main.PREFIX + "§c" + k.getName() + " §7hat vor dir die §a100 §7Level erreicht");
						k.sendMessage(Main.PREFIX + "§e" + p.getName() + " §7hatte erst §a" + level2 + " §7Level.");
						p.setLevel(0);
						if(Main.into100speedfight.contains(k.getName())) {
							p.damage(20D);
						}
					}
		    	}
		    }
		}
	}
	
    @EventHandler
    public void onFood12(FoodLevelChangeEvent e) {
    	Player p = (Player) e.getEntity();
    	if(Main.inarcherfight.contains(p.getName())) {
    		e.setCancelled(false);
		}
    }
    
    @EventHandler
    public void onBreak12(BlockBreakEvent e) {
    	Player p = e.getPlayer();
    	if(Main.inarcherfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
    
    @EventHandler
    public void onBuild12(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	if(Main.inarcherfight.contains(p.getName())) {
    		e.setCancelled(true);
		}
    }
	
	@EventHandler
	public void onIDrop12(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.inarcherfight.contains(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onIPickup12(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
    	if(Main.inarcherfight.contains(p.getName())) {
			e.setCancelled(false);
		}
	}
}
**/
