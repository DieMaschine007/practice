package de.diemaschine007.earogladiator.methods;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import net.md_5.bungee.api.ChatColor;

public class SetScoreboard_METHODS {
	public static int OnlinePlayers;
	public static int UnRankedQueuePlayers;
	public static int FightPlayers;
	public static int RankedQueuePlayers;

	public static void setScoreboard(Player p) {
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective obj = board.registerNewObjective("Ka", "Lit");
		
		Team onlineplayer = board.registerNewTeam("onlineplayer");
		Team unrankedQueue = board.registerNewTeam("unrankedQueue");
		Team fight = board.registerNewTeam("fight");
		Team line1 = board.registerNewTeam("line1");
		Team line2 = board.registerNewTeam("line2");
		Team rankedQueue = board.registerNewTeam("rankedQueue");
		
		OnlinePlayers = 0;
		UnRankedQueuePlayers = 0;
		RankedQueuePlayers = 0;
		FightPlayers = 0;


		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName("§b§lEaro §7§l⎮ §f§lPractice");
		
		obj.getScore("§7").setScore(7);
		obj.getScore("» §6Practice§7:").setScore(6);
		obj.getScore("» §6Unranked§7:").setScore(5);
		obj.getScore("» §6Ranked§7:").setScore(4);
		obj.getScore("» §6Fight§7:").setScore(3);
	    obj.getScore(ChatColor.RED.toString()).setScore(2);
	    obj.getScore("§bearomc.net").setScore(1);
		obj.getScore("§8").setScore(0);
		
		line1.addEntry("§7");
		line1.setPrefix("§7§m------------");
		line1.setSuffix("§7§m---");
		
		line2.addEntry("§8");
		line2.setPrefix("§7§m------------");
		line2.setSuffix("§7§m---");
		
		onlineplayer.addEntry("» §6Practice§7:");
		onlineplayer.setSuffix("§f  " + OnlinePlayers);
		
		unrankedQueue.addEntry("» §6Unranked§7:");
		unrankedQueue.setSuffix("§f  " + UnRankedQueuePlayers);

		rankedQueue.addEntry("» §6Ranked§7:");
		rankedQueue.setSuffix("§f  " + RankedQueuePlayers);

		fight.addEntry("» §6Fight§7:");
		fight.setSuffix("§f  " + FightPlayers);

		p.setScoreboard(board);
	}

	public static void updateScoreboard(Player p) {
		OnlinePlayers = Bukkit.getOnlinePlayers().size();

		if(p.isOnline()) {
		if(p.getScoreboard() == null) {
			setScoreboard(p);
		}
		Scoreboard board = p.getScoreboard();
		Team onlineplayer = board.getTeam("onlineplayer");
		if(onlineplayer == null) {
			onlineplayer = board.registerNewTeam("onlineplayer");
		}
		Team unrankedQueue = board.getTeam("unrankedQueue");
		if(unrankedQueue == null) {
			unrankedQueue = board.registerNewTeam("unrankedQueue");
		}
		Team fight = board.getTeam("fight");
		if(fight == null) {
			fight = board.registerNewTeam("fight");
		}
		Team rankedQueue = board.getTeam("rankedQueue");
		if(rankedQueue == null) {
			rankedQueue = board.registerNewTeam("rankedQueue");
		}

			
			onlineplayer.setSuffix("§f  " + OnlinePlayers);
			unrankedQueue.setSuffix("§f  " + UnRankedQueuePlayers);
			fight.setSuffix("§f  " + FightPlayers);
			rankedQueue.setSuffix("§f  " + RankedQueuePlayers);

			
			}
		}
			
	public static void setTab(Player p) {
		if(p.getScoreboard() == null) {
			setScoreboard(p);
		}
		Scoreboard board = p.getScoreboard();
		Team admin = board.registerNewTeam("00001Admin");
		Team dev = board.registerNewTeam("00002Dev");
		Team mod = board.registerNewTeam("00003Mod");
		Team builder = board.registerNewTeam("00004Builder");
		Team tstaff = board.registerNewTeam("00005T-Staff");
		Team commanager = board.registerNewTeam("00006ComManager");
		Team friend = board.registerNewTeam("00007Friend");
		Team media = board.registerNewTeam("00008Media");
		Team maschine = board.registerNewTeam("00009Maschine");
		Team beta = board.registerNewTeam("000091Beta");
		Team pro = board.registerNewTeam("000092Pro");
		Team player = board.registerNewTeam("000093Player");
		
		admin.setPrefix("§8[§4A§8] §4");
		dev.setPrefix("§8[§1Dev§8] §1");
		mod.setPrefix("§8[§5Mod§8] §5");
		builder.setPrefix("§8[§aB§8] §a");
		tstaff.setPrefix("§8[§dT§8] §d");
		commanager.setPrefix("§8[§2CM§8] §2");
		friend.setPrefix("§8[§fF§8] §f");
		media.setPrefix("§8[§bMedia§8] §b");
		maschine.setPrefix("§8[§eM§8] §e");
		beta.setPrefix("§8[§3Beta§8] §3");
		pro.setPrefix("§8[§6Pro§8] §6");
		player.setPrefix("§8[§7P§8] §7");
		
		for (Player all : Bukkit.getOnlinePlayers()) {
			if(all.hasPermission("simpleprefix.Admin")) {
				admin.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Dev")) {
				dev.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Mod")) {
				mod.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Builder")) {
				builder.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.TStaff")) {					
				tstaff.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.ComManager")) {					
				commanager.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Friend")) {					
				friend.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Media")) {
				media.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Maschine")) {
				maschine.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Beta")) {
				beta.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Pro")) {
				pro.addEntry(all.getName());
			} else {
				player.addEntry(all.getName());
			}
		}
	}

	public static void updateTab(Player p) {
		if(p.getScoreboard() == null) {
			setScoreboard(p);
		}
		Scoreboard board = p.getScoreboard();
		Team admin = board.getTeam("00001Admin");
		if(admin == null) {
			admin = board.registerNewTeam("00001Admin");
		}
		Team dev = board.getTeam("00002Dev");
		if(dev == null) {
			dev = board.registerNewTeam("00002Dev");
		}
		Team mod = board.getTeam("00003Mod");
		if(mod == null) {
			mod = board.registerNewTeam("00003Mod");
		}
		Team builder = board.getTeam("00004Builder");
		if(builder == null) {
			builder = board.registerNewTeam("00004Builder");
		}
		Team tstaff = board.getTeam("00005T-Staff");
		if(tstaff == null) {
			tstaff = board.registerNewTeam("00005T-Staff");
		}
		Team commanager = board.getTeam("00006ComManager");
		if(commanager == null) {
			commanager = board.registerNewTeam("00006ComManager");
		}
		Team friend = board.getTeam("00007Friend");
		if(friend == null) {
			friend = board.registerNewTeam("00007Friend");
		}
		Team media = board.getTeam("00008Media");
		if(media == null) {
			media = board.registerNewTeam("00008Media");
		}
		Team maschine = board.getTeam("00009Maschine");
		if(maschine == null) {
			maschine = board.registerNewTeam("00009Maschine");
		}
		Team beta = board.getTeam("000091Beta");
		if(beta == null) {
			beta = board.registerNewTeam("000091Beta");
		}
		Team pro = board.getTeam("000092Pro");
		if(pro == null) {
			pro = board.registerNewTeam("000092Pro");
		}
		Team player = board.getTeam("000093Player");
		if(player == null) {
			player = board.registerNewTeam("000093Player");
		}
		
		for (Player all : Bukkit.getOnlinePlayers()) {
			if(all.hasPermission("simpleprefix.Admin")) {
				admin.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Dev")) {
				dev.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Mod")) {
				mod.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Builder")) {
				builder.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.TStaff")) {					
				tstaff.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.ComManager")) {					
				commanager.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Friend")) {					
				friend.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Media")) {
				media.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Maschine")) {
				maschine.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Beta")) {
				beta.addEntry(all.getName());
			} else if (all.hasPermission("simpleprefix.Pro")) {
				pro.addEntry(all.getName());
			} else {
				player.addEntry(all.getName());
			}
		}
	}
}	
