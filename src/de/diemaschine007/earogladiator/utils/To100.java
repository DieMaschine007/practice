package de.diemaschine007.earogladiator.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class To100 {
	
	public static void run(Player p) {
		p.getInventory().clear();
		ItemStack i1 = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta m1 = i1.getItemMeta();
		m1.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
		m1.spigot().setUnbreakable(true);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(0, i1);
	}
}
