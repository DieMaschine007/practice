package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LobbyItems {
	
	public static void run(Player p) {

		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		ItemStack i1 = new ItemStack(Material.IRON_SWORD);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§2§lUnranked-Queue");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to join a Queue");
		m1.setLore(lore);
		m1.spigot().setUnbreakable(true);
		i1.setItemMeta(m1);
		
		ItemStack i2 = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta m2 = i2.getItemMeta();
		m2.setDisplayName("§9§lRanked-Queue");
		ArrayList<String> lore2 = new ArrayList<String>();
		lore2.add("§5Right Click to join a Queue");
		m2.setLore(lore2);
		m2.spigot().setUnbreakable(true);
		i2.setItemMeta(m2);
		
		ItemStack i3 = new ItemStack(Material.GHAST_TEAR);
		ItemMeta m3 = i3.getItemMeta();
		m3.setDisplayName("§b§lSpectate");
		ArrayList<String> lore3 = new ArrayList<String>();
		lore3.add("§5Right Click to spactate a Fight");
		m3.setLore(lore3);
		i3.setItemMeta(m3);
		
		ItemStack i4 = new ItemStack(Material.CHEST);
		ItemMeta m4 = i4.getItemMeta();
		m4.setDisplayName("§c§lEdit-Kit");
		i4.setItemMeta(m4);
		
		ItemStack i5 = new ItemStack(Material.SLIME_BALL);
		ItemMeta m5 = i5.getItemMeta();
		m5.setDisplayName("§a§lLeave");
		i5.setItemMeta(m5);
		
		p.getInventory().setItem(0, i1);
		p.getInventory().setItem(1, i2);
		p.getInventory().setItem(4, i3);
		p.getInventory().setItem(7, i4);
		p.getInventory().setItem(8, i5);
	}
	
}
