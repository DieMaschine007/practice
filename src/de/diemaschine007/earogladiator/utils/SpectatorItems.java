package de.diemaschine007.earogladiator.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SpectatorItems {
    public static void SetSpecItems(Player p) {
        p.getInventory().clear();
        ItemStack LeaveSpecModeItem = new ItemStack(Material.SLIME_BALL);
        ItemMeta itemMeta = LeaveSpecModeItem.getItemMeta();
        itemMeta.setDisplayName("§a§lClick to leave");
        LeaveSpecModeItem.setItemMeta(itemMeta);
        Inventory inv = p.getInventory();
        inv.clear();
        inv.setItem(8, LeaveSpecModeItem);
        p.updateInventory();
    }
}
