package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LeaveQueueItem {
	
	public static void run(Player p) {
		p.getInventory().clear();
		ItemStack i1 = new ItemStack(Material.REDSTONE);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§c§lLeave Queue");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to leave Queue!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(4, i1);
	}
	
}
