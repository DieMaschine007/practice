package de.diemaschine007.earogladiator.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Utils_Shop {
	
	public static void fillInventory(Inventory inv, ItemStack filling) {
		for(int i = 0; i < inv.getSize(); i++) {
			inv.setItem(i, filling);
		}
	}
	public static void setItemInInv(Inventory inv, ItemStack it, int place) {
		inv.setItem(place, it);
	}
	
	public static void openQueue(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9*3, "§bQueue");
		fillInventory(inv, Items.createItemStack(Material.STAINED_GLASS_PANE, 1, 7, " "));
		setItemInInv(inv, Items.createItemStack(Material.DIAMOND_SWORD, 1, 0, "§5Feast-Kit"), 0);
		setItemInInv(inv, Items.createItemStack(Material.ANVIL, 1, 0, "§7Anchor-Kit"), 1);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 26);
		
		p.openInventory(inv);
	}
	
	public static void openSettings(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9*1, "§5Settings");
		fillInventory(inv, Items.createItemStack(Material.STAINED_GLASS_PANE, 1, 7, " "));
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 0);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 4);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 8);
		
		p.openInventory(inv);
	}
	
	public static void openUnranked(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9*2, "§2Unranked-Queue");
		fillInventory(inv, Items.createItemStack(Material.STAINED_GLASS_PANE, 1, 7, " "));
		setItemInInv(inv, Items.createItemStack(Material.MUSHROOM_SOUP, 1, 0, "§aFeast"), 0);
		setItemInInv(inv, Items.createItemStack(Material.LAVA_BUCKET, 1, 0, "§aBuildUHC"), 1);
		setItemInInv(inv, Items.createItemStack(Material.POTION, 1, 16421, "§aNoDebuff"), 2);
		setItemInInv(inv, Items.createItemStack(Material.GOLDEN_APPLE, 1, 1, "§aGapple"), 3);
		setItemInInv(inv, Items.createItemStack(Material.STONE_SWORD, 1, 0, "§aEarlyHG"), 4);
		setItemInInv(inv, Items.createItemStack(Material.FISHING_ROD, 1, 0, "§aSG"), 5);
		setItemInInv(inv, Items.createItemStack(Material.POTION, 1, 16388, "§aDebuff"), 6);
		setItemInInv(inv, Items.createItemStack(Material.BOW, 1, 0, "§aArcher"), 7);
		setItemInInv(inv, Items.createItemStack(Material.BEACON, 1, 0, "§aSoupWars1vs1"), 8);
		setItemInInv(inv, Items.createItemStack(Material.LEASH, 1, 0, "§aSumo"), 9);
		setItemInInv(inv, Items.createItemStack(Material.BEDROCK, 1, 0, "§aTo100"), 10);
		setItemInInv(inv, Items.createItemStack(Material.POTION, 1, 8226, "§aTo100-Speed"), 11);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 17);
		
		p.openInventory(inv);
	}
	
	public static void openRanked(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9*2, "§9Ranked-Queue");
		fillInventory(inv, Items.createItemStack(Material.STAINED_GLASS_PANE, 1, 7, " "));
		setItemInInv(inv, Items.createItemStack(Material.MUSHROOM_SOUP, 1, 0, "§aFeast"), 0);
		setItemInInv(inv, Items.createItemStack(Material.LAVA_BUCKET, 1, 0, "§aBuildUHC"), 1);
		setItemInInv(inv, Items.createItemStack(Material.POTION, 1, 16421, "§aNoDebuff"), 2);
		setItemInInv(inv, Items.createItemStack(Material.LEASH, 1, 0, "§aSumo"), 3);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 17);
		
		p.openInventory(inv);
	}
	
	public static void openSpec(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9*5, "§bSpectate");
		fillInventory(inv, Items.createItemStack(Material.STAINED_GLASS_PANE, 1, 7, " "));
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena1"), 0);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena2"), 1);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena3"), 2);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena4"), 3);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena5"), 4);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena6"), 5);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena7"), 6);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena8"), 7);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena9"), 8);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena10"), 9);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena11"), 10);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena12"), 11);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena13"), 12);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena14"), 13);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena15"), 14);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena16"), 15);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena17"), 16);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena18"), 17);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena19"), 18);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena20"), 19);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena21"), 20);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena22"), 21);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena23"), 22);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena24"), 23);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena25"), 24);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena26"), 25);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena27"), 26);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena28"), 27);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena29"), 28);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena30"), 29);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena31"), 30);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena32"), 31);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena33"), 32);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena34"), 33);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena35"), 34);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena36"), 35);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena37"), 36);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena38"), 37);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena39"), 38);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena40"), 39);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena41"), 40);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena42"), 41);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena43"), 42);
		setItemInInv(inv, Items.createItemStack(Material.IRON_BLOCK, 1, 0, "§fArena44"), 43);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 44);
		p.openInventory(inv);
	}
	
	public static void openEditKit(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9*1, "§cEdit-Kit");
		fillInventory(inv, Items.createItemStack(Material.STAINED_GLASS_PANE, 1, 7, " "));
		setItemInInv(inv, Items.createItemStack(Material.MUSHROOM_SOUP, 1, 0, "§cEdit: Feast"), 0);
		setItemInInv(inv, Items.createItemStack(Material.LAVA_BUCKET, 1, 0, "§cEdit: BuildUHC"), 1);
		setItemInInv(inv, Items.createItemStack(Material.POTION, 1, 16421, "§cEdit: NoDebuff"), 2);
		setItemInInv(inv, Items.createItemStack(Material.STONE_SWORD, 1, 0, "§cEdit: EarlyHG"), 3);
		setItemInInv(inv, Items.createItemStack(Material.FISHING_ROD, 1, 0, "§cEdit: SG"), 4);
		setItemInInv(inv, Items.createItemStack(Material.POTION, 1, 16388, "§cEdit: Debuff"), 5);
		setItemInInv(inv, Items.createItemStack(Material.BEACON, 1, 0, "§cEdit: SoupWars1vs1"), 6);
		setItemInInv(inv, Items.createItemStack(Material.BOW, 1, 0, "§cEdit: Archer"), 7);
		setItemInInv(inv, Items.createItemStack(Material.REDSTONE, 1, 0, "§cClose Inv"), 8);
		
		p.openInventory(inv);
	}
}