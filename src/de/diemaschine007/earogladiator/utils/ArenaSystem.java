/*
package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import de.diemaschine007.earogladiator.methods.PlayerMode;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.diemaschine007.earogladiator.main.Main;

public class ArenaSystem {
	public static ArrayList<Player> Arena = new ArrayList<>();
	public static void onArrayClear(Player p) {
		Main.lobby.remove(p.getName());
		Main.inarcherfight.remove(p.getName());
		Main.inarcherqueue.remove(p.getName());
		Main.inbuilduhcfight.remove(p.getName());
		Main.inbuilduhcqueue.remove(p.getName());
		Main.indebufffight.remove(p.getName());
		Main.indebuffqueue.remove(p.getName());
		Main.inearlyhgfight.remove(p.getName());
		Main.inearlyhgqueue.remove(p.getName());
		Main.infeastfight.remove(p.getName());
		Main.infeastqueue.remove(p.getName());
		Main.ingapplefight.remove(p.getName());
		Main.ingapplequeue.remove(p.getName());
		Main.innodebufffight.remove(p.getName());
		Main.innodebuffqueue.remove(p.getName());
		Main.insgfight.remove(p.getName());
		Main.insgqueue.remove(p.getName());
		Main.insoupwarsfight.remove(p.getName());
		Main.insoupwarsqueue.remove(p.getName());
		Main.insumofight.remove(p.getName());
		Main.insumoqueue.remove(p.getName());
		Main.into100fight.remove(p.getName());
		Main.into100queue.remove(p.getName());
		Main.into100speedfight.remove(p.getName());
		Main.into100speedqueue.remove(p.getName());
	}
	
	public static void startFight(Player player) {
		PlayerMode.playermode.put(player, PlayerMode.FIGHT_MODE());
	}
	public static void onFeastTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
		p.getInventory().clear();
		p.teleport(Pos1);
		Main.lobby.remove(p.getName());
		Main.infeastfight.add(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
		Feast.run(p);
		for(String allplayers : Main.infeastqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.lobby.remove(player.getName());
			Main.infeastqueue.remove(player.getName());
			Main.infeastfight.add(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			Feast.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onBuildUHCTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.inbuilduhcfight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
        BuildUHC.run(p);
		for(String allplayers : Main.inbuilduhcqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.inbuilduhcfight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.inbuilduhcqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			BuildUHC.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onNoDebuffTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.innodebufffight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
		NoDebuff.run(p);
		for(String allplayers : Main.innodebuffqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.innodebufffight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.innodebuffqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			NoDebuff.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	public static void onGappleTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects()) {
			p.removePotionEffect(effect.getType());
		}

        p.getInventory().clear();
		p.teleport(Pos1);
		Main.ingapplefight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
		Gapple.run(p);
		for(String allplayers : Main.ingapplequeue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.ingapplefight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.ingapplequeue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			Gapple.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onEarlyHGTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.inearlyhgfight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
		EarlyHG.run(p);
		for(String allplayers : Main.inearlyhgqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.inearlyhgfight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.inearlyhgqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			EarlyHG.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onSgTp(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.insgfight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
        SG.run(p);
		for(String allplayers : Main.insgqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.insgfight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.insgqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
            SG.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onDebuffTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.indebufffight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
		Debuff.run(p);
		for(String allplayers : Main.indebuffqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.indebufffight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.indebuffqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			Debuff.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onSumoTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.insumofight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
		for(String allplayers : Main.insumoqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.insumofight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.insumoqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onto100TP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.into100fight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
        To100.run(p);
		for(String allplayers : Main.into100queue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.into100fight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.into100queue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
            To100.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
	
	public static void onto100SpeedTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.into100speedfight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
        To100.run(p);
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000 * 20, 1));
		for(String allplayers : Main.into100speedqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.into100speedfight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.into100speedqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
            To100.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 10000 * 20, 1));
		}
	}
	
	public static void onArcherTP(Player p, ArrayList<Player> Arena, Location Pos1, Location Pos2) {
		for (PotionEffect effect : p.getActivePotionEffects())
			p.removePotionEffect(effect.getType());
        p.getInventory().clear();
		p.teleport(Pos1);
		Main.inarcherfight.add(p.getName());
		Main.lobby.remove(p.getName());
		Arena.add(p);
        p.setFireTicks(0);
        p.setHealth(20D);
        Archer.run(p);
		for(String allplayers : Main.inarcherqueue) {
			Player player = Bukkit.getPlayer(allplayers);
            player.getInventory().clear();
			player.teleport(Pos2);
			Main.inarcherfight.add(player.getName());
			Main.lobby.remove(player.getName());
			Main.inarcherqueue.remove(player.getName());
			Arena.add(player);
            player.setFireTicks(0);
            player.setHealth(20D);
            Archer.run(player);
			for (PotionEffect effect : player.getActivePotionEffects())
				player.removePotionEffect(effect.getType());
		}
	}
}
*/
