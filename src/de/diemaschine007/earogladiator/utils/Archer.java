package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Archer {
	
	public static void run(Player p) {
		ItemStack i1 = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§a§lArcher");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to use the Kit!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(8, i1);
	}
	
	public static void run2(Player p) {
		ItemStack i1 = new ItemStack(Material.WOOD_PICKAXE);
		ItemMeta m1 = i1.getItemMeta();
		i1.setItemMeta(m1);
		
		ItemStack i2 = new ItemStack(Material.BOW, 1);
		ItemMeta m2 = i2.getItemMeta();
		m2.addEnchant(Enchantment.ARROW_DAMAGE, 2, true);
		m2.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		i2.setItemMeta(m2);
		
		ItemStack i3 = new ItemStack(Material.CHAINMAIL_HELMET);
		ItemMeta m3 = i3.getItemMeta();
		m3.spigot().setUnbreakable(true);
		i3.setItemMeta(m3);
		
		ItemStack i4 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta m4 = i4.getItemMeta();
		m4.spigot().setUnbreakable(true);
		i4.setItemMeta(m4);
		
		ItemStack i5 = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ItemMeta m5 = i5.getItemMeta();
		m5.spigot().setUnbreakable(true);
		i5.setItemMeta(m5);
		
		ItemStack i6 = new ItemStack(Material.CHAINMAIL_BOOTS);
		ItemMeta m6 = i6.getItemMeta();
		m6.spigot().setUnbreakable(true);
		i6.setItemMeta(m6);
		
		ItemStack i7 = new ItemStack(Material.ARROW, 1);
		ItemMeta m7 = i7.getItemMeta();
		i7.setItemMeta(m7);
		
		ItemStack i8 = new ItemStack(Material.GOLDEN_CARROT, 64);
		ItemMeta m8 = i8.getItemMeta();
		i8.setItemMeta(m8);
		
		p.getInventory().setItem(0, i1);
		p.getInventory().setItem(1, i2);
		p.getInventory().setItem(8, i7);
		p.getInventory().setItem(7, i8);	
		
		p.getInventory().setBoots(i6);
		p.getInventory().setLeggings(i5);
		p.getInventory().setChestplate(i4);
		p.getInventory().setHelmet(i3);
	}
}
