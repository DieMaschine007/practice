package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Gapple {
	
	public static void run(Player p) {
		ItemStack i1 = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§a§lGapple");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to use the Kit!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(8, i1);
	}
	
	public static void run2(Player p) {

		ItemStack i1 = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta m1 = i1.getItemMeta();
		m1.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
		m1.addEnchant(Enchantment.FIRE_ASPECT, 2, true);
		m1.spigot().setUnbreakable(true);
		i1.setItemMeta(m1);
		
		ItemStack i2 = new ItemStack(Material.GOLDEN_APPLE, 64, (short) 1);
		ItemMeta m2 = i2.getItemMeta();
		i2.setItemMeta(m2);
		
		ItemStack i3 = new ItemStack(Material.DIAMOND_HELMET);
		ItemMeta m3 = i3.getItemMeta();
		m3.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		i3.setItemMeta(m3);
		
		ItemStack i4 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta m4 = i4.getItemMeta();
		m4.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		i4.setItemMeta(m4);
		
		ItemStack i5 = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemMeta m5 = i5.getItemMeta();
		m5.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		i5.setItemMeta(m5);
		
		ItemStack i6 = new ItemStack(Material.DIAMOND_BOOTS);
		ItemMeta m6 = i6.getItemMeta();
		m6.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		m6.addEnchant(Enchantment.PROTECTION_FALL, 4, true);
		i6.setItemMeta(m6);
		
		ItemStack i7 = new ItemStack(Material.POTION, 1);
		ItemMeta m7 = i7.getItemMeta();
		m7.setDisplayName("§dMystical Potion");
		i7.setItemMeta(m7);
		p.getInventory().clear();
		p.getInventory().setItem(0, i1);
		p.getInventory().setItem(1, i2);
		p.getInventory().setItem(2, i3);
		p.getInventory().setItem(3, i4);
		p.getInventory().setItem(4, i5);
		p.getInventory().setItem(5, i6);
		p.getInventory().setItem(6, i7);
		p.getInventory().setItem(7, null);
		p.getInventory().setItem(8, null);
		
		p.getInventory().setBoots(i6);
		p.getInventory().setLeggings(i5);
		p.getInventory().setChestplate(i4);
		p.getInventory().setHelmet(i3);
	}
}
