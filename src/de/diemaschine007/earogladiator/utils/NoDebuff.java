package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NoDebuff {
	
	public static void run(Player p) {
		ItemStack i1 = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§a§lNoDebuff");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to use the Kit!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(8, i1);
	}
	
	public static void run2(Player p) {
		p.getInventory().clear();
		ItemStack i1 = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta m1 = i1.getItemMeta();
		m1.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
		m1.addEnchant(Enchantment.FIRE_ASPECT, 2, true);
		m1.spigot().setUnbreakable(true);
		i1.setItemMeta(m1);
		
		ItemStack i2 = new ItemStack(Material.POTION, 1, (short) 16421);
		ItemMeta m2 = i2.getItemMeta();
		i2.setItemMeta(m2);
		
		ItemStack i3 = new ItemStack(Material.DIAMOND_HELMET);
		ItemMeta m3 = i3.getItemMeta();
		m3.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		m3.spigot().setUnbreakable(true);
		i3.setItemMeta(m3);
		
		ItemStack i4 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta m4 = i4.getItemMeta();
		m4.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		m4.spigot().setUnbreakable(true);
		i4.setItemMeta(m4);
		
		ItemStack i5 = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemMeta m5 = i5.getItemMeta();
		m5.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		m5.spigot().setUnbreakable(true);
		i5.setItemMeta(m5);
		
		ItemStack i6 = new ItemStack(Material.DIAMOND_BOOTS);
		ItemMeta m6 = i6.getItemMeta();
		m6.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		m6.addEnchant(Enchantment.PROTECTION_FALL, 4, true);
		m6.spigot().setUnbreakable(true);
		i6.setItemMeta(m6);
		
		ItemStack i7 = new ItemStack(Material.POTION, 1, (short) 8259);
		ItemMeta m7 = i7.getItemMeta();
		i7.setItemMeta(m7);
		
		ItemStack i8 = new ItemStack(Material.POTION, 1, (short) 8226);
		ItemMeta m8 = i8.getItemMeta();
		i8.setItemMeta(m8);
		
		ItemStack i9 = new ItemStack(Material.GOLDEN_CARROT, 64);
		ItemMeta m9 = i9.getItemMeta();
		i9.setItemMeta(m9);
		
		ItemStack i10 = new ItemStack(Material.ENDER_PEARL, 16);
		ItemMeta m10 = i10.getItemMeta();
		i10.setItemMeta(m10);
		
		p.getInventory().setItem(0, i1);
		p.getInventory().setItem(1, i10);
		p.getInventory().setItem(2, i2);
		p.getInventory().setItem(3, i2);
		p.getInventory().setItem(4, i2);
		p.getInventory().setItem(5, i2);
		p.getInventory().setItem(6, i7);
		p.getInventory().setItem(7, i8);
		p.getInventory().setItem(8, i9);
		
		p.getInventory().setItem(9, i2);
		p.getInventory().setItem(10, i2);
		p.getInventory().setItem(11, i2);
		p.getInventory().setItem(12, i2);
		p.getInventory().setItem(13, i2);
		p.getInventory().setItem(14, i2);
		p.getInventory().setItem(15, i2);
		p.getInventory().setItem(16, i2);
		p.getInventory().setItem(17, i8);
		
		p.getInventory().setItem(18, i2);
		p.getInventory().setItem(19, i2);
		p.getInventory().setItem(20, i2);
		p.getInventory().setItem(21, i2);
		p.getInventory().setItem(22, i2);
		p.getInventory().setItem(23, i2);
		p.getInventory().setItem(24, i2);
		p.getInventory().setItem(25, i2);
		p.getInventory().setItem(26, i8);
		
		p.getInventory().setItem(27, i2);
		p.getInventory().setItem(28, i2);
		p.getInventory().setItem(29, i2);
		p.getInventory().setItem(30, i2);
		p.getInventory().setItem(31, i2);
		p.getInventory().setItem(32, i2);
		p.getInventory().setItem(33, i2);
		p.getInventory().setItem(34, i2);
		p.getInventory().setItem(35, i8);
		
		p.getInventory().setBoots(i6);
		p.getInventory().setLeggings(i5);
		p.getInventory().setChestplate(i4);
		p.getInventory().setHelmet(i3);
	}
}
