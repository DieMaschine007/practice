package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SG {
	
	public static void run(Player p) {
		ItemStack i1 = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§a§lSG");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to use the Kit!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(8, i1);
	}
	
	public static void run2(Player p) {
		ItemStack i1 = new ItemStack(Material.STONE_SWORD);
		ItemMeta m1 = i1.getItemMeta();
		m1.spigot().setUnbreakable(true);
		i1.setItemMeta(m1);
		
		ItemStack i2 = new ItemStack(Material.FISHING_ROD, 1);
		ItemMeta m2 = i2.getItemMeta();
		i2.setItemMeta(m2);
		
		ItemStack i3 = new ItemStack(Material.IRON_HELMET);
		ItemMeta m3 = i3.getItemMeta();
		m3.spigot().setUnbreakable(true);
		i3.setItemMeta(m3);
		
		ItemStack i4 = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta m4 = i4.getItemMeta();
		m4.spigot().setUnbreakable(true);
		i4.setItemMeta(m4);
		
		ItemStack i5 = new ItemStack(Material.IRON_LEGGINGS);
		ItemMeta m5 = i5.getItemMeta();
		m5.spigot().setUnbreakable(true);
		i5.setItemMeta(m5);
		
		ItemStack i6 = new ItemStack(Material.IRON_BOOTS);
		ItemMeta m6 = i6.getItemMeta();
		m6.spigot().setUnbreakable(true);
		i6.setItemMeta(m6);
		
		ItemStack i7 = new ItemStack(Material.BOW, 1);
		ItemMeta m7 = i7.getItemMeta();
		i7.setItemMeta(m7);
		
		ItemStack i8 = new ItemStack(Material.GOLDEN_APPLE, 1);
		ItemMeta m8 = i8.getItemMeta();
		i8.setItemMeta(m8);
		
		ItemStack i9 = new ItemStack(Material.COOKED_BEEF, 3);
		ItemMeta m9 = i9.getItemMeta();
		i9.setItemMeta(m9);
		
		ItemStack i10 = new ItemStack(Material.BREAD, 64);
		ItemMeta m10 = i10.getItemMeta();
		i10.setItemMeta(m10);
		
		ItemStack i11 = new ItemStack(Material.ARROW, 5);
		ItemMeta m11 = i11.getItemMeta();
		i11.setItemMeta(m11);
		
		ItemStack i12 = new ItemStack(Material.WEB, 2);
		ItemMeta m12 = i12.getItemMeta();
		i12.setItemMeta(m12);
		
		ItemStack i13 = new ItemStack(Material.FIREBALL, 3);
		ItemMeta m13 = i13.getItemMeta();
		i13.setItemMeta(m13);

		p.getInventory().clear();
		p.getInventory().setItem(0, i1);
		p.getInventory().setItem(1, i2);
		p.getInventory().setItem(2, i7);
		p.getInventory().setItem(3, i8);
		p.getInventory().setItem(4, i9);
		p.getInventory().setItem(5, i10);
		p.getInventory().setItem(6, i11);
		p.getInventory().setItem(7, i12);
		p.getInventory().setItem(8, i13);
		
		
		p.getInventory().setBoots(i6);
		p.getInventory().setLeggings(i5);
		p.getInventory().setChestplate(i4);
		p.getInventory().setHelmet(i3);
	}
}
