package de.diemaschine007.earogladiator.utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class Items {
	public static ItemStack createItemStack(Material mat, int amount, int shortid, String DisplayName) {
		short s = (short) shortid;
		ItemStack i = new ItemStack(mat, amount, s);
		ItemMeta meta = i.getItemMeta();
		meta.setDisplayName(DisplayName);
		i.setItemMeta(meta);
		return i;
	}
	
	public static ItemStack createLeather(Material mat, int amount, int shortid, Color color, String DisplayName) {
		short s = (short) shortid;
		ItemStack i = new ItemStack(mat, amount, s);
		LeatherArmorMeta meta = (LeatherArmorMeta) i.getItemMeta();
		meta.setDisplayName(DisplayName);
		meta.setColor(color);
		i.setItemMeta(meta);
		return i;
	}
	
	public static ItemStack createItem2(Material mat, int amount, int shortid) {
		short s = (short) shortid;
		ItemStack i = new ItemStack(mat, amount, s);
		ItemMeta meta = i.getItemMeta();
		i.setItemMeta(meta);
		return i;
	}
}
