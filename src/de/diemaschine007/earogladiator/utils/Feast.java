package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Feast {
	
	public static void run(Player p) {
		p.getInventory().clear();
		ItemStack i1 = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§a§lFeast");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to use the Kit!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(8, i1);
	}
	
	public static void run2(Player p) {
		ItemStack SwordItem = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta m1 = SwordItem.getItemMeta();
		m1.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
		m1.spigot().setUnbreakable(true);
		SwordItem.setItemMeta(m1);
		
		ItemStack SoupItem = new ItemStack(Material.MUSHROOM_SOUP);
		ItemMeta m2 = SoupItem.getItemMeta();
		SoupItem.setItemMeta(m2);
		
		ItemStack IronHelmetItem = new ItemStack(Material.IRON_HELMET);
		ItemMeta m3 = IronHelmetItem.getItemMeta();
		m3.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		IronHelmetItem.setItemMeta(m3);
		
		ItemStack IronChestPlateItem = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta m4 = IronChestPlateItem.getItemMeta();
		m4.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		IronChestPlateItem.setItemMeta(m4);
		
		ItemStack IronLeggingsItem = new ItemStack(Material.IRON_LEGGINGS);
		ItemMeta m5 = IronLeggingsItem.getItemMeta();
		m5.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		IronLeggingsItem.setItemMeta(m5);
		
		ItemStack IronBootsItem = new ItemStack(Material.IRON_BOOTS);
		ItemMeta m6 = IronBootsItem.getItemMeta();
		m6.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		IronBootsItem.setItemMeta(m6);
		
		ItemStack RedMushroomItem = new ItemStack(Material.RED_MUSHROOM, 32);
		ItemMeta m7 = RedMushroomItem.getItemMeta();
		RedMushroomItem.setItemMeta(m7);
		
		ItemStack BowlItem = new ItemStack(Material.BOWL, 32);
		ItemMeta m8 = BowlItem.getItemMeta();
		BowlItem.setItemMeta(m8);
		
		ItemStack BrownMushroomItem = new ItemStack(Material.BROWN_MUSHROOM, 32);
		ItemMeta m9 = BrownMushroomItem.getItemMeta();
		BrownMushroomItem.setItemMeta(m9);
		
		p.getInventory().setItem(0, SwordItem);

		for (int i = 1; i<13; i++) {
			p.getInventory().setItem(i, SoupItem);
		}

		p.getInventory().setItem(13, RedMushroomItem);
		p.getInventory().setItem(14, BowlItem);
		p.getInventory().setItem(15, BrownMushroomItem);

		for (int i = 16; i<36; i++) {
			p.getInventory().setItem(i, SoupItem);
		}

		p.getInventory().setBoots(IronBootsItem);
		p.getInventory().setLeggings(IronLeggingsItem);
		p.getInventory().setChestplate(IronChestPlateItem);
		p.getInventory().setHelmet(IronHelmetItem);
	}
}
