package de.diemaschine007.earogladiator.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class EarlyHG {
	
	public static void run(Player p) {
		ItemStack i1 = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta m1 = i1.getItemMeta();
		m1.setDisplayName("§a§lEarlyHG");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("§5Right Click to use the Kit!");
		m1.setLore(lore);
		i1.setItemMeta(m1);
		
		p.getInventory().setItem(8, i1);
	}
	
	public static void run2(Player p) {
		ItemStack i1 = new ItemStack(Material.STONE_SWORD);
		ItemMeta m1 = i1.getItemMeta();
		m1.spigot().setUnbreakable(true);
		i1.setItemMeta(m1);
		
		ItemStack i2 = new ItemStack(Material.MUSHROOM_SOUP);
		ItemMeta m2 = i2.getItemMeta();
		i2.setItemMeta(m2);
		p.getInventory().clear();
		p.getInventory().setItem(0, i1);
		p.getInventory().setItem(1, i2);
		p.getInventory().setItem(2, i2);
		p.getInventory().setItem(3, i2);
		p.getInventory().setItem(4, i2);
		p.getInventory().setItem(5, i2);
		p.getInventory().setItem(6, i2);
		p.getInventory().setItem(7, i2);
		p.getInventory().setItem(8, i2);
		
		p.getInventory().setItem(9, i2);
		p.getInventory().setItem(10, i2);
		p.getInventory().setItem(11, i2);
		p.getInventory().setItem(12, i2);
		p.getInventory().setItem(13, i2);
		p.getInventory().setItem(14, i2);
		p.getInventory().setItem(15, i2);
		p.getInventory().setItem(16, i2);
		p.getInventory().setItem(17, i2);
		
		p.getInventory().setItem(18, i2);
		p.getInventory().setItem(19, i2);
		p.getInventory().setItem(20, i2);
		p.getInventory().setItem(21, i2);
		p.getInventory().setItem(22, i2);
		p.getInventory().setItem(23, i2);
		p.getInventory().setItem(24, i2);
		p.getInventory().setItem(25, i2);
		p.getInventory().setItem(26, i2);
		
		p.getInventory().setItem(27, i2);
		p.getInventory().setItem(28, i2);
		p.getInventory().setItem(29, i2);
		p.getInventory().setItem(30, i2);
		p.getInventory().setItem(31, i2);
		p.getInventory().setItem(32, i2);
		p.getInventory().setItem(33, i2);
		p.getInventory().setItem(34, i2);
		p.getInventory().setItem(35, i2);
		
	}
}
