package de.diemaschine007.earogladiator.commands;

import de.diemaschine007.earogladiator.main.Main;
import de.diemaschine007.earogladiator.main.PlayerMode;
import de.diemaschine007.earogladiator.utils.SpectatorItems;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpecCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (command.getName().equals("spec")) {
            if (commandSender instanceof Player) {
                Player p = (Player) commandSender;
                if (args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        if (target.getName() != p.getName()) {
                            if (!PlayerMode.isInSpectatorMode(target)) {
                                spectatePlayer(p, target);
                            }
                        } else {
                            p.sendMessage(Main.PREFIX + "§cDu kannst dich nicht selbst spectaten!");
                        }
                    } else {
                        p.sendMessage(Main.PREFIX + "§cDer Spieler ist nicht online!");
                    }
                } else {
                    p.sendMessage(Main.PREFIX + "§cBenutze /spec <playername>");
                }
            }
        }
        return false;
    }

    public static void spectatePlayer(Player spectator, Player target) {
        if (!de.diemaschine007.earogladiator.main.PlayerMode.playermode.get(spectator).equals(de.diemaschine007.earogladiator.main.PlayerMode.SPECTATOR_MODE) && PlayerMode.isInLobbyMode(spectator) && PlayerMode.isInFightMode(spectator)) {
            PlayerMode.setPlayerMode(spectator, PlayerMode.SPECTATOR_MODE);

            for (Player players : Bukkit.getOnlinePlayers()) {
                players.hidePlayer(spectator);
            }
            Location location = target.getLocation();
            location.setY(location.getY() + 10);
            spectator.setAllowFlight(true);
            spectator.setFlying(true);

            SpectatorItems.SetSpecItems(spectator);

            spectator.teleport(location);
            spectator.sendMessage(Main.PREFIX + "Du schaust nun §6" + target.getName() + " §7beim Schwitzen zu.");
        } else {
            spectator.sendMessage(Main.PREFIX + "Du bist schon im Spectator Modus.");
        }


    }


}
