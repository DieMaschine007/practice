package de.diemaschine007.earogladiator.commands;

import de.diemaschine007.earogladiator.arenasystem.ArenaManager;
import de.diemaschine007.earogladiator.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class EloCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command label, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (args.length == 0) {
                int currentElo;
                currentElo = ArenaManager.playerEloMap.get(player);
                player.sendMessage(Main.PREFIX + "Dein Elo ist §6" + currentElo);
            } else {
                if (args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        int currentElo;
                        currentElo = ArenaManager.playerEloMap.get(target);
                        player.sendMessage(Main.PREFIX + "Das Elo von " + target.getName() +  " ist §6" + currentElo);
                    } else {
                        player.sendMessage(Main.PREFIX + "§cDer Spieler ist nicht online!");
                    }
                } else {
                    player.sendMessage(Main.PREFIX + "§cBitte benutze /elo [<playername>]");
                }

            }
        }
        return false;
    }
}
