package de.diemaschine007.earogladiator.commands;

import de.diemaschine007.earogladiator.main.PlayerMode;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.diemaschine007.earogladiator.main.Main;

public class Spawn_Command implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("spawn")) {
			Location loc = new Location(Bukkit.getWorld("practice"), 33.5, 29, 22.5);
			if(sender instanceof Player) {
				final Player p = (Player) sender;
				final Location loc1 = p.getLocation();
					if(args.length == 0) {
						if(!PlayerMode.isInLobbyMode(p)) {
							return true;
						} else {
							p.sendMessage(Main.PREFIX + "§7Du wirst in 2 Sekunden teleportiert. §cNicht bewegen!");
							Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
								
								@Override
								public void run() {
									
									final Location loc2 = p.getLocation();
									
									if(loc1.equals(loc2)) {
										p.sendMessage(Main.PREFIX + "§7Du wurdest zum §4Spawn §7teleportiert.");
										p.teleport(loc);
									} else {
										p.sendMessage(Main.PREFIX + "§cDa du dich bewegt hast wurdest du nicht teleportiert.");
									}
								}
							}, 20L*2);
						}
					} else if(args.length != 0) {
						p.sendMessage(Main.PREFIX + "§cBenutze /spawn");
				}
			}
		}
		return false;
	}
}
